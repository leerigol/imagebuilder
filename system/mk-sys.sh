#!/bin/sh
#make kernel and rootfs package

out=system.img
fs=rootfs
dt=system
in=system.its

gzip -c $fs.img > $fs.gz
#./tree
dtc -I dts -O dtb -o $dt.dtb $dt.dts

#2018.02.28 by hexiaohua. remove rsa signature
#mkimage -D "-I dts -O dtb -p 2000" -K rsa-keys/flamingo-gel-key.dtb -k rsa-keys/ -f $in  -r $out 1>/dev/null
mkimage -D "-I dts -O dtb -p 2000"  -f $in  -r $out 1>/dev/null

mv $out ../
rm $dt.dtb
rm $fs.gz
