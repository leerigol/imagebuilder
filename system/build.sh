
#compile kernel
if [ ! $1 ]; then
	cd ~/workspace/flamingo/kernel/linux-3.12.0
	#make clean
	make -j8
	cp arch/arm/boot/zImage ../../images/system/

	cp ./drivers/usb/gadget/usbtmc_dev.ko ../../images/rigol/drivers/

	cp -a ./drivers/usb/class/usbtmc.ko ../../images/rigol/drivers/usb_gpib.ko
	cp ./drivers/hwmon/adc128d818.ko ../../images/rigol/drivers/
	cp ./drivers/hwmon/tmp421.ko ../../images/rigol/drivers/

	cp ./drivers/usb/gadget/libcomposite.ko ../../images/rigol/drivers/
	cp ./drivers/usb/gadget/usb_f_ss_lb.ko ../../images/rigol/drivers/

	cp ./drivers/usb/gadget/usbtmc_dev.ko ~/workspace/nfs/rigol/drivers/
	cp ./drivers/usb/gadget/libcomposite.ko ~/workspace/nfs/rigol/drivers/
	cp ./drivers/usb/gadget/usb_f_ss_lb.ko ~/workspace/nfs/rigol/drivers/

	cp ./drivers/hwmon/adc128d818.ko ~/workspace/nfs/rigol/drivers/
	cp ./drivers/hwmon/tmp421.ko ~/workspace/nfs/rigol/drivers/

	cd ../devIRQ/
	./build.sh
	
	cd ../axi
	./build.sh

	cd ~/workspace/flamingo/images/system/


fi

out=system.img
dt=nfs
in=nfs.its
dt252=252
it252=252.its

#compile the two devicetree
dtc -I dts -O dtb -o $dt.dtb $dt.dts



#DTC_OPS="-I dts -O dtb -p 2000"
#cp rsa-keys/flamingo-gel-key.dtb flamingo-gel-key.dtb
#mkimage -D "-I dts -O dtb -p 2000" -K rsa-keys/flamingo-gel-key.dtb -f $in -k rsa-keys/ -r $out 1>/dev/null
mkimage -D "-I dts -O dtb -p 2000"  -f $in -r $out 1>/dev/null

#mkimage tool to integrate the zImage and corresponding devicetree.dtb
#mkimage -f kernel.its kernel.itb
#dtc -I dtb -O dts -o pub.dts flamingo-gel-key.dtb
mv $out ~/workspace/nfs
rm $dt.dtb


#dtc -I dts -O dtb -o $dt252.dtb $dt252.dts
#mkimage -D "-I dts -O dtb -p 2000" -K rsa-keys/flamingo-gel-key.dtb -f $it252 -k rsa-keys/ -r $out 1>/dev/null
#mkimage -D "-I dts -O dtb -p 2000"  -f $it252 -r $out 1>/dev/null
#mv $out 252_$out
#rm $dt252.dtb

