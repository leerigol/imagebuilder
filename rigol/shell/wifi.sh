#!/bin/sh

#input1 SSID
#input2 PASS

############################################################
#make sure the driver is OK for 8812AU
############################################################
lsmod | grep 8812 >/dev/null 2>&1
if [ $? -ne 0 ]; then
	insmod /rigol/8812au.ko >/dev/null 2>&1
fi

ifconfig wlan0 >/dev/null 2>&1
if [ $? -ne 0 ]; then
	echo "Wifi module not exist"
	exit 1
fi

ifconfig wlan0 up >/dev/null 2>&1

WPA_SOCKET_FILE=/var/run/wpa.conf
############################################################
#kill wpa_supplicant process
############################################################
ps -o comm | grep wpa_supplicant
if [ $? -eq 0 ]; then
	wpa_cli -iwlan0 -p$WPA_SOCKET_FILE disc
	wpa_cli -iwlan0 -p$WPA_SOCKET_FILE term
	#kill -9 $(ps -o comm -o pid | grep wpa_supplicant | awk '{print $2}')
fi

############################################################
#make connection config
############################################################
WIFI_CONF=/rigol/data/wifi.conf
WIFI_SSID=$1
WIFI_PASS=$2
WIFI_TEMP=/tmp/wifi.txt

rm $WIFI_CONF >/dev/null 2>&1

echo "ctrl_interface="$WPA_SOCKET_FILE >> $WIFI_CONF
echo "network={"				>>	$WIFI_CONF
echo "ssid=\"$WIFI_SSID\"" 		>>	$WIFI_CONF

############################################################
#no password
############################################################
if [ ! $WIFI_PASS ]; then
	echo "key_mgmt=NONE" 		>> $WIFI_CONF
else
	echo "psk=\"$WIFI_PASS\""	>>	$WIFI_CONF
fi
echo "}"						>>	$WIFI_CONF

############################################################
#connecting without log
############################################################
#echo 0 > /proc/sys/kernel/printk
/sbin/wpa_supplicant -iwlan0 -c$WIFI_CONF -B

############################################################
#waiting for physical connection
############################################################
connect=0
for i in $(seq 10);                                                                        
do                                                                                         
	iwconfig wlan0 | grep unassociated
	if [ $? -eq 0 ]; then
		sleep 1
	else
		connect=1
		break
	fi
done 

if [ $connect -eq 0 ]; then
	echo "Connect failed"
	exit 2
else	
	echo "OK"
fi

