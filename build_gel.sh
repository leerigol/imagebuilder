#!/bin/bash

model=MSO7054
softver=01.01.01.07.04 #01-2017,02-2018,03-2019...
builddate=$(date "+%Y-%m-%d %H:%M:%S")
#builddate='2018-04-27 13:22:29'
vendor='RIGOL TECHNOLOGIES'
update_set='/rigol/tools/cfger -s'

build_linux=fw4linux.sh.unaes
build_uboot=fw4uboot.sh.unaes
build_linux_aes=fw4linux.sh
build_uboot_aes=fw4uboot.sh

build_cmd=./cfger
build_env=/tmp/env.bin
build_ds7=DS7000Update.GEL
build_ds8=DS8000Update.GEL

uboot_msg='Upgrading firmware,please waitting'

#build_dat=cal.hex
file_logo=logo.hex
file_zynq=zynq.bit
file_sys=system.img
file_app=app.img
file_boot=boot.bin

build_index=(0 1 2 3)
build_name=($file_logo 	$file_zynq 	$file_sys 	$file_app)
build_mtds=('$log_mtd' '$bit_mtd' 	'$sys_mtd' 	'$app_mtd')
#partition mtd offset
build_offs=('${log_offset}' '${bit_offset}' '${sys_offset}'	'${app_offset}')
#partition mtd size
build_size=(0x400000	0x800000	0x2000000	0x6400000)

build_exec=0x2000000	#The memory addr of the update shell.32MiB

build_from=0x4000000	#The memory addr of the update package.64MiB
build_temp=0xA000000	#The memory addr of the gz file.160MiB
build_file=0x10000000	#The memory addr of the update file.256MiB

#for boot shell
nand_log1=0x4500000
nand_bit1=0x4900000
nand_sys1=0x5100000
nand_app1=0x7100000		#The offset of the app1 partition
nand_log2=0xd500000
nand_bit2=0xd900000
nand_sys2=0xe100000
nand_app2=0x10100000	#The offset of the app2 partition

boot_addr=0x3000000		#The starting address of the system
build_boot=0			#The offset of the QSPI for bootloader

if [ -f $build_linux ]; then
	rm $build_linux
fi

if [ -f $build_uboot ]; then
	rm $build_uboot
fi

linux_addMd5()
{
	name=$1
	name=${name%.*} #remove .gz
	name=${name%.*} #remove .ext

	temp=$(md5sum $1)
	temp=${temp%  *}
	echo $name"_md5='"$temp"'" >>      $build_linux
}


temp_pkg=pkg.tar
linux_addContent()
{	
	tar -rf $temp_pkg $1
	#rm $1
}

###################################################################################
# p1 the memory addr of the update package
# p2 the memory addr of the temp update file
# p3 the name the gz update file
# p4 the md5
###################################################################################
uboot_addMd5()
{
	echo 'if tar '$1 $2 $3'; then \'					>> $build_uboot
	echo '	if md5sum -v '$2' ${temp_file_size} '$4'; then \'>> $build_uboot
	echo '	    echo check '$3' md5 success; \'			>> $build_uboot
	echo '	else \'										>> $build_uboot
	echo '	    echo check '$3' md5 error; \'			>> $build_uboot
	echo '    	setenv errmsg "Check md5 failed";\' 	>> $build_uboot
	echo '		exit_from_shell;\' 						>> $build_uboot
	echo '	fi; \'										>> $build_uboot
	echo 'else \'										>> $build_uboot
	echo '	echo tar '$3' error !;\'					>> $build_uboot
	echo '  setenv errmsg "File parse failed(-1)";\' 	>> $build_uboot
	echo '	exit_from_shell;\' 							>> $build_uboot
	echo 'fi;'											>> $build_uboot
}

###################################################################################
# p1 the memory addr of the update package
# p2 the memory addr of the gz update file
# p3 the name the update file
# p4 the offset of the partition
# p5 the size of the partition
# p6 the memory addr of the update file
###################################################################################
uboot_addUpdate()
{
	echo 'if tar' $1 $2 $3'; then \' 					>> $build_uboot
	echo '   if unzip' $2 $6'; then \'					>> $build_uboot
	echo '      nand erase '$4 $5';\' 					>> $build_uboot
	echo '      nand write '$6 $4' ${temp_file_size};\'	>> $build_uboot
	echo '   else \' 									>> $build_uboot	
	echo '      echo unzip '$3' error !;\'				>> $build_uboot
	echo '  	setenv errmsg "File parse failed(-2)";\'>> $build_uboot
	echo '		exit_from_shell;\' 						>> $build_uboot
	echo '   fi; \' 									>> $build_uboot
	echo 'else \' 										>> $build_uboot
	echo '	echo tar '$3' error !;\' 					>> $build_uboot
	echo '	setenv errmsg "File parse failed(-3)"; \' 	>> $build_uboot
	echo '	exit_from_shell;\' 							>> $build_uboot
	echo 'fi;' 											>> $build_uboot
}

###################################################################################
# add message shown on LCD for uboot shell
###################################################################################
uboot_addMessage()
{
	uboot_msg=$uboot_msg.
	echo "showMessage '"$uboot_msg"';beeper;"			>> $build_uboot
}
###################################################################################
# p1 the memory addr of the update package
# p2 the memory addr of the gz update file
# p3 the name the update file
# p4 the offset of the partition
# p5 the memory addr of the update file
###################################################################################
uboot_upgrade_bootloader()
{
	echo 'if tar' $1 $2 $3'; then \' 					>> $build_uboot
	echo '   if unzip' $2 $5'; then \'					>> $build_uboot
	echo '      storage qspi; \'						>> $build_uboot
	echo '      if sf probe; then \'					>> $build_uboot
	echo '         sf update '$5 $4' ${temp_file_size};\'>> $build_uboot
	echo '      else \' 								>> $build_uboot	
	echo '         echo cannot find qspi \'				>> $build_uboot
	printf '       showMessage "sf %s failed";\' $3		>> $build_uboot
	echo '		   exit_from_shell;\' 					>> $build_uboot
	echo '      fi; \' 									>> $build_uboot
	echo '   else \' 									>> $build_uboot	
	echo '      echo uzip '$3' error !;\'				>> $build_uboot
	printf '    showMessage "unzip %s failed";\' $3		>> $build_uboot
	echo '		exit_from_shell;\' 						>> $build_uboot
	echo '   fi; \' 									>> $build_uboot
	echo 'else \' 										>> $build_uboot
	echo '    echo tar '$3' error !;\' 					>> $build_uboot
	printf '  showMessage "tar %s failed";\' $3			>> $build_uboot
	echo '	  exit_from_shell;\' 						>> $build_uboot
	echo 'fi;' 											>> $build_uboot
	
}

uboot_selectApp()
{
	echo 'if test ${bootpart} = A; then \'				>> $build_uboot
    echo '  echo update app2;\' 					    >> $build_uboot
	echo "	setenv log_offset '"$nand_log2"'; "'\'		>> $build_uboot
	echo "	setenv bit_offset '"$nand_bit2"'; "'\'		>> $build_uboot
	echo "	setenv sys_offset '"$nand_sys2"'; "'\'		>> $build_uboot
	echo "	setenv app_offset '"$nand_app2"'; "'\'		>> $build_uboot
	echo "	setenv bootpart 'B'; "'\'					>> $build_uboot
	echo "	setenv backpart 'A'; "'\'					>> $build_uboot
	echo 'else \'										>> $build_uboot
    echo '  echo update app1;\' 					    >> $build_uboot
	echo "	setenv log_offset '"$nand_log1"'; "'\'		>> $build_uboot
	echo "	setenv bit_offset '"$nand_bit1"'; "'\'		>> $build_uboot
	echo "	setenv sys_offset '"$nand_sys1"'; "'\'		>> $build_uboot
	echo "	setenv app_offset '"$nand_app1"'; "'\'		>> $build_uboot
	echo "	setenv bootpart 'A'; "'\'					>> $build_uboot
	echo "	setenv backpart 'B'; "'\'					>> $build_uboot
	echo 'fi;'											>> $build_uboot
}

####################################################################################
#build system
####################################################################################
echo 'building system.img'
cd system
./mk-sys.sh
cd ../

####################################################################################
#build app
####################################################################################
echo 'building app.img'
cd app
./mk512M.sh #HDV5
#./mk1G.sh  #HDV4
cd ../

####################################################################################
#creating uboot update shell
####################################################################################
echo 'creating uboot update shell'
echo '#upgrade_shell'					>> $build_uboot
echo '#check version first'				>> $build_uboot

echo 'if checkVer "'$softver'"; then \'	>> $build_uboot
echo '	echo New version; \'			>> $build_uboot
echo 'else \'							>> $build_uboot
echo ' echo Old version; \'				>> $build_uboot
echo ' setenv bootparam "0x44454654"; \'>> $build_uboot
#echo '	setenv errmsg "Invalid version(-4)"; \'>> $build_uboot
#echo '	exit_from_shell;\'				>> $build_uboot	
echo 'fi;'								>> $build_uboot

echo ''									>> $build_uboot


####################################################################################
#add md5sum for fw4uboot.sh
####################################################################################
echo ''									>> $build_uboot
echo '#check all md5' 					>> $build_uboot
#add message
uboot_addMessage

for i in ${build_index[@]}
do	
	filename=${build_name[$i]}	
	if [ -f $filename ]; then
		gzip $filename -c > $filename.gz		
		md5=$(md5sum -b $filename.gz | cut -d ' ' -f1)
		uboot_addMd5 $build_from $build_temp $filename.gz $md5
	fi	
done
#add boot md5

####################################################################################
#add app select
####################################################################################
echo '#select app'						>> $build_uboot
uboot_selectApp;
echo ''									>> $build_uboot


####################################################################################
#add image update/Upgrading firmware,please waiting
####################################################################################
echo '#update' 							>> $build_uboot

for i in ${build_index[@]}
do
	uboot_addMessage
	filename=${build_name[$i]}	
	if [ -f $filename ]; then
		uboot_addUpdate $build_from $build_temp $filename.gz ${build_offs[$i]} ${build_size[$i]} $build_file
	fi	
done

####################################################################################
#env
####################################################################################
echo '#env' 							>> $build_uboot
#echo "setenv model   '"$model"'" 		>> $build_uboot
#echo 'setenv bootver ${bootver}'		>> $build_uboot
echo "setenv softver '"$softver"'" 		>> $build_uboot
echo "setenv builddate '"$builddate"'" 	>> $build_uboot

echo ''									>> $build_uboot

uboot_addMessage
if [ -f $file_sys ]; then
	size=$(stat -c "%s" $file_sys) #
	printf 'setenv nandboot${bootpart} \"checkGTP;loadzynq ${bit_offset};ledoff;loadlogo ${log_offset};nand read 0x%x ${sys_offset} 0x%x;bootm 0x%x"\n' \
	$boot_addr  $size $boot_addr	>> $build_uboot
	printf 'setenv bootlogo loadlogo ${log_offset}\n' >> $build_uboot
fi

printf 'setenv bootcmd   "if run nandboot${bootpart}; then echo ok; else setenv bootpart ${backpart};save;run nandboot${backpart}; fi"\n' \
	>> $build_uboot

#clear temp env
echo ''									>> $build_uboot
echo "#clear"							>> $build_uboot
echo "setenv log_offset;"				>> $build_uboot
echo "setenv bit_offset;"				>> $build_uboot
echo "setenv sys_offset;"				>> $build_uboot
echo "setenv app_offset;"				>> $build_uboot
echo "setenv temp_file_size;"			>> $build_uboot
echo "saveenv;"							>> $build_uboot
echo "##end##"							>> $build_uboot

#----------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------#
#creating linux update shell
#----------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------#
echo 'creating linux update shell'

echo '#!/bin/sh' 					>> $build_linux
echo ''			 					>> $build_linux

echo 'model='$model 				>> $build_linux
echo 'softver='$softver 			>> $build_linux
echo 'builddate="'$builddate'"'		>> $build_linux
echo ''			 					>> $build_linux


#----------------------------------------------------------------------------------#
#add common functions
#----------------------------------------------------------------------------------#

cat linux_inc.sh					>> $build_linux


printf 'linux_checkPackage;\n' 		>> $build_linux
printf 'linux_checkHeader;\n\n' 	>> $build_linux

printf 'echo "Checking md5"\n' 		>> $build_linux
for i in ${build_index[@]}
do
	filename=${build_name[$i]}	
	if [ -f $filename ]; then
		#gzip $filename -c > $filename.gz		
		md5=$(md5sum -b $filename.gz | cut -d ' ' -f1)
		printf "linux_checkMd5 %s '%s';\n" $filename $md5	>> $build_linux
	fi	
done


printf '\nlinux_selectApp;\n' 		>> $build_linux


for i in ${build_index[@]}
do
	filename=${build_name[$i]}	
	if [ -f $filename ]; then
		printf 'linux_update %s %s;\n' ${build_name[$i]} ${build_mtds[$i]}	>> $build_linux
	fi	
done


if [ -f $file_sys ]; then
	size=$(stat -c "%s" $file_sys)
	printf '\nlinux_setBoot 0x%x $bootfrom 0x%x $zynqfrom $logofrom;\n' $boot_addr $size >> $build_linux
fi

echo 	'#set misc'									>> $build_linux
echo	$update_set builddate '"'$builddate'"'		>> $build_linux
printf 'linux_setMisc;\n'                           >> $build_linux
printf 'linux_writeFlash %s /dev/mtd0;\n' $build_env>> $build_linux


printf 'echo "OK"\n'  								>> $build_linux


#----------------------------------------------------------------------------------#
#build update package
#----------------------------------------------------------------------------------#
echo 'building package'


#----------------------------------------------------------------------------------#
#add update shell
#----------------------------------------------------------------------------------#
$build_cmd -e $build_linux $build_linux_aes
$build_cmd -e $build_uboot $build_uboot_aes

#cp $build_uboot $build_uboot_aes
linux_addContent $build_linux_aes;
linux_addContent $build_uboot_aes;
mv $build_linux $build_linux_aes
mv $build_uboot $build_uboot_aes

#----------------------------------------------------------------------------------#
#add update partition
#----------------------------------------------------------------------------------#
for i in ${build_index[@]}
do
	filename=${build_name[$i]}	
	if [ -f $filename ]; then
		linux_addContent $filename.gz;
		rm $filename.gz
	fi
done


rm $build_ds7 >/dev/null 2>&1
mv $temp_pkg $build_ds7
#rm $build_linux

