#Make a JFFS2 file system image from an existing directory tree

#Options:
#  -p, --pad[=SIZE]        Pad output to SIZE bytes with 0xFF. If SIZE is
#                          not specified, the output is padded to the end of
#                          the final erase block
#  -r, -d, --root=DIR      Build file system from directory DIR (default: cwd)
#  -s, --pagesize=SIZE     Use page size (max data node size) SIZE.
#                          Set according to target system's memory management
#                          page size (default: 4KiB)
#  -e, --eraseblock=SIZE   Use erase block size SIZE (default: 64KiB)
#  -c, --cleanmarker=SIZE  Size of cleanmarker (default 12)
#  -m, --compr-mode=MODE   Select compression mode (default: priority)
#  -x, --disable-compressor=COMPRESSOR_NAME
#                          Disable a compressor
#  -X, --enable-compressor=COMPRESSOR_NAME
#                          Enable a compressor
#  -y, --compressor-priority=PRIORITY:COMPRESSOR_NAME
#                          Set the priority of a compressor
#  -L, --list-compressors  Show the list of the available compressors
#  -t, --test-compression  Call decompress and compare with the original (for test)
#  -n, --no-cleanmarkers   Don't add a cleanmarker to every eraseblock
#  -o, --output=FILE       Output to FILE (default: stdout)
#  -l, --little-endian     Create a little-endian filesystem
#  -b, --big-endian        Create a big-endian filesystem
#  -D, --devtable=FILE     Use the named FILE as a device table file
#  -f, --faketime          Change all file times to '0' for regression testing
#  -q, --squash            Squash permissions and owners making all files be owned by root
#  -U, --squash-uids       Squash owners making all files be owned by root
#  -P, --squash-perms      Squash permissions on all files
#  -h, --help              Display this help text
#  -v, --verbose           Verbose operation
#  -V, --version           Display version information
#  -i, --incremental=FILE  Parse FILE and generate appendage output for it
#  page  size = 2048B
#  erase size = 128KiB
mkfs.jffs2  -n -s 2048 -e 128KiB -d data/ -o data.img
 

