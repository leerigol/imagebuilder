#!/bin/sh

echo 'sync scpi message value'
cd ~/workspace/flamingo/tool/scpiTool
#echo 'NO COMPILE SCPI'
./start.sh
cd ~/workspace/flamingo/images/app/

#copy data
rm ./app/resource/* -fdr
#cp ~/workspace/flamingo/framework/resource/* ~/workspace/nfs/rigol/resource/ -fdr  #>/dev/null 2>&1
cp ~/workspace/flamingo/framework/resource/* ./app/resource/ -fdr #>/dev/null 2>&1

#cp ~/workspace/flamingo/framework/meta/platform_a/* ~/workspace/nfs/rigol/resource/ #>/dev/null 2>&1
cp ~/workspace/flamingo/framework/meta/platform_a/* ./app/resource/ #>/dev/null 2>&1

cp ../rigol/*   ./app/ -fdr #>/dev/null 2>&1
#cp ../rigol/* ~/workspace/nfs/rigol/ -fdr #>/dev/null 2>&1

rm ./app/resource/pictures -fdr
rm ./app/resource/fonts -fdr >/dev/null #2>&1

########1.create root ubi volume
#-m pagesize=2048
#peb=128Kib
#-e leb = peb - 2 x pagesize
#size=100Mib
#-c max count = 100Mib/leb
mkfs.ubifs -F -r ./app -m 2KiB -e 124KiB -c 825 -o app.vol 


########2. create ubifs
#-o app.ubi output file name
#-m size of page
#-p size of block
ubinize -o app.ubi -m 2KiB -p 128KiB 512ubinize.cfg

cp app.ubi ../app.img
