#!/bin/sh

echo 'sync scpi message value'
~/workspace/flamingo/tool/scpiTool/start.sh >/dev/null 2>&1

#copy data
cp ~/workspace/flamingo/framework/resource/* ~/workspace/nfs/rigol/resource/ -fdr  #>/dev/null 2>&1

rm ./app/resource/* -fdr
cp ~/workspace/flamingo/framework/resource/* ./app/resource/ -fdr #>/dev/null 2>&1

rm ./app/resource/fonts -fdr >/dev/null #2>&1

cp ~/workspace/flamingo/framework/meta/platform_a/* ~/workspace/nfs/rigol/resource/ #>/dev/null 2>&1
cp ~/workspace/flamingo/framework/meta/platform_a/* ./app/resource/ #>/dev/null 2>&1

cp ../rigol/*   ./app/ -fdr #>/dev/null 2>&1
cp ../rigol/* ~/workspace/nfs/rigol/ -fdr #>/dev/null 2>&1

#1. create root ubi volume
#-m pagesize=4096
#peb=256Kib
#-e leb = peb - 2 x pagesize
#size=100Mib
#-c max count = 100Mib/leb
mkfs.ubifs -F -r ./app -m 4KiB -e 248KiB -c 412 -o app.vol 


########2. create ubifs
#-o app.ubi output file name
#-m size of page
#-p size of block
ubinize -o app.ubi -m 4KiB -p 256KiB ubinize.cfg

cp app.ubi ../app.img
