
############################update shell for system and app ############################
update_cmp='/rigol/tools/cfger -c'	#0=equal,1=large 2=small
update_set='/rigol/tools/cfger -s'

update_package=$1


#******************************************************
#Error code define
#1 Invalid package
#2 Parse package failed
#3 Version is too low
#******************************************************

########################################################
#1. check package exist
#2. check md5
########################################################
linux_checkPackage()
{
	if [ ! $update_package ]; then
		echo 'Invalid package'
		exit 1
	fi

	if [ ! -f $update_package ]; then
		echo 'Invalid package'
		exit 1
	fi
}

########################################################
#1. check md5
########################################################
linux_checkMd5()
{	
	#echo $1 $2	
	#exist the update file	
	tar -xf $update_package $1.gz
	if [ $? -eq 0 ]; then
		echo $2'  '$1.gz | md5sum -c >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			rm $1.gz >/dev/null 2>&1
			echo 'check md5 failure for' $1
			exit 2
		fi
	else
		echo 'err:' $1 'not exist'
		exit 2
	fi
	rm $1.gz >/dev/null 2>&1
}

########################################################
#1. check model
#2. check software version
########################################################
linux_checkHeader()
{
	#determine the version. less or equal
	$update_cmp softver $softver
	if [ $? -eq 1 ]; then
		echo 'Version is too low'
		exit 3;
	fi
}

########################################################
#1. select app mtd
########################################################
linux_selectApp()
{
	$update_cmp bootpart A
	if [ $? -eq 0 ]; then #exist
		#echo 'Updating app B'
		bootpart='B'
		backpart='A'
		log_mtd=/dev/mtd7
		bit_mtd=/dev/mtd8
		sys_mtd=/dev/mtd9
		app_mtd=/dev/mtd10
		bootfrom=0xe100000		#The offset of the system partition offset(kernel,rootfs)
		logofrom=0xd500000
		zynqfrom=0xd900000
	else
		#echo 'Updating app A'	
		bootpart='A'
		backpart='B'
		log_mtd=/dev/mtd3
		bit_mtd=/dev/mtd4
		sys_mtd=/dev/mtd5
		app_mtd=/dev/mtd6
		bootfrom=0x5100000		#The offset of the system partition offset(kernel,rootfs)
		logofrom=0x4500000
		zynqfrom=0x4900000
	fi
}

########################################################
#. set the boot command shell
# p1= starting memory address of the system
# p2= the offset of system partition
# p3= the size of the system partition
# p4= the offset of the bit partition
# p5= the offset of the logo partition
# p6= the offset of the other system partition
########################################################
linux_setBoot()
{
#
	$update_set nandboot$bootpart "checkGTP;loadzynq $4;ledoff;loadlogo $5;nand read $1 $2 $3;bootm $1"
	$update_set bootlogo  "loadlogo $5"
}

########################################################
#1. set new software
#2. set app mtd
#3. set boot device
########################################################
linux_setMisc()
{
	$update_set softver   $softver
	$update_set bootpart  $bootpart	
	#$update_set bootcmd   "run nandboot$bootpart"
	$update_set bootcmd   "if run nandboot$bootpart; then echo 'ok'; else setenv bootpart $backpart;save;run nandboot$backpart; fi"
}

########################################################
#1. xxx.img
#2. /dev/mtdx
########################################################
linux_writeFlash()
{
	echo "Upgrading:" $1 
	flash_eraseall $2    >/dev/null 2>&1
	nandwrite -p   $2 $1 >/dev/null 2>&1
	#echo $1 '>>' $2
	
}

########################################################
#1. xxx.img
#2. /dev/mtdx
########################################################
linux_update()
{
	tar -xOf $update_package $1.gz | gunzip -c >$1
	if [ $? -eq 0 ]; then
		linux_writeFlash $1 $2;
		rm $1
	else
		echo $1 'not exist'
		exit 2
	fi
}

####################################################################################
#update start from here
####################################################################################
