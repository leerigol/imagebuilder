#include <stdio.h>
#include <string.h>
#include <linux/input.h>
#include <fcntl.h>
#include <sys/time.h>
#include <unistd.h>

int reportkey(int fd, unsigned short type, unsigned short code, int value)  
{  
    struct input_event event;  

    event.type = type;  
    event.code = code;  
    event.value = value;  

    gettimeofday(&event.time, 0);  

    if (write(fd, &event, sizeof(struct input_event)) < 0) 
	{  
        printf("report key error!\n");  
        return -1;  
    }  

    return 0;  
}  


void main(void)
{
	int	dev_fd = open("/dev/input/event0", O_RDWR);
	if( dev_fd >= 0 )
	{
#if 0
		simulate_mouse( dev_fd, 30,30 );
		simulate_key(dev_fd, BTN_LEFT);  //模拟按下鼠标左键
#else
		reportkey(dev_fd,EV_KEY,BTN_TOUCH,1); //report touch preesed event
		//reportkey(dev_fd,EV_ABS,ABS_MT_TRACKING_ID,0);
		//reportkey(dev_fd,EV_ABS,ABS_MT_TOUCH_MAJOR,1);

		reportkey(dev_fd,EV_ABS,ABS_MT_POSITION_X,148);
		reportkey(dev_fd,EV_ABS,ABS_MT_POSITION_Y,549); //report position x,y

		//reportkey(dev_fd,EV_ABS,ABS_MT_WIDTH_MAJOR,1); //report preeusre 200
		
		reportkey(dev_fd,EV_SYN,SYN_MT_REPORT,0); //report syn signal , finish the curent event!
		reportkey(dev_fd,EV_SYN,SYN_REPORT,0); //report syn signal , finish the curent event!

usleep(100000);

		reportkey(dev_fd,EV_KEY,BTN_TOUCH,0); //report touch preesed event
		reportkey(dev_fd,EV_ABS,ABS_MT_TRACKING_ID,0);
		reportkey(dev_fd,EV_ABS,ABS_MT_TOUCH_MAJOR,0);

		reportkey(dev_fd,EV_ABS,ABS_MT_POSITION_X,148);
		reportkey(dev_fd,EV_ABS,ABS_MT_POSITION_Y,549); //report position x,y

		reportkey(dev_fd,EV_ABS,ABS_MT_WIDTH_MAJOR,0); //report preeusre 200
		
		reportkey(dev_fd,EV_SYN,SYN_MT_REPORT,0); //report syn signal , finish the curent event!
		reportkey(dev_fd,EV_SYN,SYN_REPORT,0); //report syn signal , finish the curent event!
#endif
		printf("touch by soft\n");
		close(dev_fd);
	}
	
}
