#!/bin/sh

model=MSO7014
softver=00.01.00.03.04
builddate="2017.03.25 17:32:52"


############################update shell for system and app ############################
update_cmp='/rigol/cfger -c'	#0=equal,1=large 2=small
update_set='/rigol/cfger -s'

update_package=$1
app_mtd=/dev/mtd6

########################################################
#1. check package exist
#2. check md5
########################################################
linux_checkPackage()
{
	if [ ! $update_package ]; then
		echo 'no update package'
		exit 1
	fi

	if [ ! -f $update_package ]; then
		echo 'update package not exist'
		exit 1
	fi
}

########################################################
#1. check md5
########################################################
linux_checkMd5()
{	
	#echo $1 $2	
	#exist the update file	
	tar -xf $update_package $1.gz
	if [ $? -eq 0 ]; then
		echo $2'  '$1.gz | md5sum -c >/dev/null 2>&1
		if [ $? -ne 0 ]; then
			rm $1.gz >/dev/null 2>&1
			echo 'check md5 failure for' $1
			exit 1
		fi
	else
		echo 'err:' $1 'not exist'
		exit 1
	fi
	rm $1.gz >/dev/null 2>&1
}

########################################################
#1. check model
#2. check software version
########################################################
linux_checkHeader()
{
	#determine the version. less or equal
	$update_cmp softver $softver
	if [ $? -eq 1 ]; then
		echo 'version is not valid'
		exit 1;
	fi
}

########################################################
#1. select app mtd
########################################################
linux_selectApp()
{
	$update_cmp bootpart A
	if [ $? -eq 0 ]; then #exist
		#echo 'Updating app B'
		app_mtd=/dev/mtd7
		bootpart='B'
	else
		#echo 'Updating app A'	
		bootpart='A'
	fi
}

########################################################
#. set the boot command shell
# p1= starting memory address of the system
# p2= the offset of system partition
# p3= the size of the system partition
########################################################
linux_setBoot()
{
#loadzynq;
	$update_set nandboot "loadzynq;run bootlogo;nand read $1 $2 $3;bootm $1"
}

########################################################
#1. set new software
#2. set app mtd
#3. set boot device
########################################################
linux_setMisc()
{
	$update_set softver   $softver
	$update_set builddate $builddate
	$update_set bootpart  $bootpart
	$update_set bootcmd   "run nandboot"
}

########################################################
#1. xxx.img
#2. /dev/mtdx
########################################################
linux_writeFlash()
{
	echo "Upgrading:" $1 
	flash_eraseall $2    >/dev/null 2>&1
	nandwrite -p   $2 $1 >/dev/null 2>&1
	#echo $1 '>>' $2
	
}

########################################################
#1. xxx.img
#2. /dev/mtdx
########################################################
linux_update()
{
	tar -xOf $update_package $1.gz | gunzip -c >$1
	if [ $? -eq 0 ]; then
		linux_writeFlash $1 $2;
		rm $1
	else
		echo $1 'not exist'
	fi
}

####################################################################################
#update start from here
####################################################################################
linux_checkPackage;
linux_checkHeader;

echo "Checking md5"
linux_checkMd5 logo.hex '4c2c431b2861897df6fdb701dafa5b66';
linux_checkMd5 zynq.bit '8620197eb4aeca99aa5a8c06e937aa0d';
linux_checkMd5 system.img 'ce96b90dd18dcadee10095a54cca2378';
linux_checkMd5 app.img '0c692269519589e6dad6d7ef39f0525f';

linux_selectApp;
linux_update logo.hex /dev/mtd1;
linux_update zynq.bit /dev/mtd3;
linux_update system.img /dev/mtd4;
linux_update app.img $app_mtd;

linux_setBoot 0x3000000 0x1100000 0xd966ca;
linux_setMisc;
linux_writeFlash /tmp/env.bin /dev/mtd0;
echo "OK"
