
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "../aes.h"

#define BUFSIZE 0x2000
extern unsigned char AES_KEY[16];

unsigned char BUF[BUFSIZE];

void usage(void)
{
	printf("aes -e input output  for encrypt \n \
			aes -d input output  for decrypt\n");			      
}

int main( int argc, char*argv[] )
{
    int keysize = 128;
    unsigned char tmp_key[16];
 	unsigned char __tmp[BUFSIZE];
    FILE *inputFd, *outputFd;
    
    unsigned int file_len = 0;
    
    
    unsigned int  aes_type;
    aes_context aes;

	if(argc != 4)
	{
			usage();
			exit(-1);
	}
	
	if(strncmp(argv[1], "-e",2) == 0)
	{
			aes_type = AES_ENCRYPT;
	}
	else if(strncmp(argv[1] , "-d", 2) == 0)
	{
			aes_type = AES_DECRYPT;
	}
	else
	{
		  usage();
			exit(-1);
	}
	
	if(strlen(argv[2]) == 0)
	{
			usage();
			exit(-1);
	}
	
	if(strlen(argv[3]) == 0)
	{
			usage();
			exit(-1);
	}
	
	inputFd = fopen(argv[2], "r");
	
	if(inputFd == NULL)
	{
			printf("Can't open %s\n", argv[2]);
			exit(-1);
	}
	
	outputFd = fopen(argv[3], "w+");
	
	if(outputFd == NULL)
	{
			printf("Can't open %s\n", argv[3]);
			exit(-1);
	}

	memset(__tmp, 0, sizeof(__tmp));
	memset(BUF,   0xa, sizeof(BUF));
	if(aes_type == AES_ENCRYPT)
	{
		aes_setkey_enc(&aes, AES_KEY, keysize);
		memcpy(tmp_key, AES_KEY, sizeof(AES_KEY));

		do
		{
			file_len = fread(BUF, 1,BUFSIZE,inputFd);
			if(file_len == 0)
			{
				 break;
			}

			if(file_len % 16)
			{
				file_len =  (file_len + 15) & 0xfffffff0;
			}

			if(0 != aes_crypt_cbc(&aes, AES_ENCRYPT, file_len, tmp_key, BUF, __tmp) )
			{
			   printf("Encrypt failure\n");
			   fclose(inputFd);
			   fclose(outputFd);
			   exit(-1);
			}
			printf("file size = %d\n", file_len);
			if(file_len != fwrite(__tmp, 1, file_len, outputFd) )
			{
			   printf("Write file failure\n");
			   fclose(inputFd);
			   fclose(outputFd);
			   exit(-1);
			}
		}while(file_len == BUFSIZE);
		
		fclose(inputFd);
		fclose(outputFd);			
	}
	else if(aes_type == AES_DECRYPT)
	{
		unsigned char __tmp[BUFSIZE];
		aes_setkey_dec(&aes, AES_KEY, keysize);
		memcpy(tmp_key, AES_KEY, sizeof(AES_KEY));
		do
		{
			 file_len = fread(BUF, 1,BUFSIZE,inputFd);
			 if(file_len == 0)
			 {
			 	break;
			 }
			 
			 if(0 != aes_crypt_cbc(&aes, AES_DECRYPT, file_len, tmp_key, BUF, __tmp) )
			 {
			 	   printf("Encrypt failure\n");
			 	   fclose(inputFd);
			 	   fclose(outputFd);
			 	   exit(-1);
			 }
			 
			 if(file_len != fwrite(__tmp, 1, file_len, outputFd) )
			 {
			 	 	 printf("Write file failure\n");
			 	   fclose(inputFd);
			 	   fclose(outputFd);
			 	   exit(-1);
			 }
		}while(file_len == BUFSIZE);

		fclose(inputFd);
		fclose(outputFd);			
	}
}
