#update information
#default offset of app
setenv app_offset '0x7100000'

#check all md5
if tar 0x4000000 0xA000000 logo.hex.gz; then \
	if md5sum -v 0xA000000 ${temp_file_size} 4c2c431b2861897df6fdb701dafa5b66; then \
	    echo check logo.hex.gz md5 success; \
	else \
	    echo check logo.hex.gz md5 error; \
    showMessage "md5 logo.hex.gz failed";exit_from_shell;\
	fi; \
else \
	echo tar logo.hex.gz error !;\
    showMessage "tar logo.hex.gz failed";exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 zynq.bit.gz; then \
	if md5sum -v 0xA000000 ${temp_file_size} 8620197eb4aeca99aa5a8c06e937aa0d; then \
	    echo check zynq.bit.gz md5 success; \
	else \
	    echo check zynq.bit.gz md5 error; \
    showMessage "md5 zynq.bit.gz failed";exit_from_shell;\
	fi; \
else \
	echo tar zynq.bit.gz error !;\
    showMessage "tar zynq.bit.gz failed";exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 system.img.gz; then \
	if md5sum -v 0xA000000 ${temp_file_size} ce96b90dd18dcadee10095a54cca2378; then \
	    echo check system.img.gz md5 success; \
	else \
	    echo check system.img.gz md5 error; \
    showMessage "md5 system.img.gz failed";exit_from_shell;\
	fi; \
else \
	echo tar system.img.gz error !;\
    showMessage "tar system.img.gz failed";exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 app.img.gz; then \
	if md5sum -v 0xA000000 ${temp_file_size} 0c692269519589e6dad6d7ef39f0525f; then \
	    echo check app.img.gz md5 success; \
	else \
	    echo check app.img.gz md5 error; \
    showMessage "md5 app.img.gz failed";exit_from_shell;\
	fi; \
else \
	echo tar app.img.gz error !;\
    showMessage "tar app.img.gz failed";exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 boot.bin.gz; then \
	if md5sum -v 0xA000000 ${temp_file_size} bb016090d7840b878cd1fdaf90b050ff; then \
	    echo check boot.bin.gz md5 success; \
	else \
	    echo check boot.bin.gz md5 error; \
    showMessage "md5 boot.bin.gz failed";exit_from_shell;\
	fi; \
else \
	echo tar boot.bin.gz error !;\
    showMessage "tar boot.bin.gz failed";exit_from_shell;\
fi;

#select app
if test ${bootpart} = A; then \
  echo update app2;\
	setenv app_offset '0xd500000'; \
	setenv bootpart 'B'; \
else \
  echo update app1;\
	setenv bootpart 'A'; \
fi;

#update
if tar 0x4000000 0xA000000 logo.hex.gz; then \
   if unzip 0xA000000 0x10000000; then \
      			beeper;\
      nand erase 0x100000 0x400000;\
      nand write 0x10000000 0x100000 ${temp_file_size};\
   else \
      echo unzip logo.hex.gz error !;\
    showMessage "unzip logo.hex.gz failed";exit_from_shell;\
   fi; \
else \
    echo tar logo.hex.gz error !;\
  showMessage "tar logo.hex.gz failed";exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 zynq.bit.gz; then \
   if unzip 0xA000000 0x10000000; then \
      			beeper;\
      nand erase 0x900000 0x800000;\
      nand write 0x10000000 0x900000 ${temp_file_size};\
   else \
      echo unzip zynq.bit.gz error !;\
    showMessage "unzip zynq.bit.gz failed";exit_from_shell;\
   fi; \
else \
    echo tar zynq.bit.gz error !;\
  showMessage "tar zynq.bit.gz failed";exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 system.img.gz; then \
   if unzip 0xA000000 0x10000000; then \
      			beeper;\
      nand erase 0x1100000 0x2000000;\
      nand write 0x10000000 0x1100000 ${temp_file_size};\
   else \
      echo unzip system.img.gz error !;\
    showMessage "unzip system.img.gz failed";exit_from_shell;\
   fi; \
else \
    echo tar system.img.gz error !;\
  showMessage "tar system.img.gz failed";exit_from_shell;\
fi;
if tar 0x4000000 0xA000000 app.img.gz; then \
   if unzip 0xA000000 0x10000000; then \
      			beeper;\
      nand erase ${app_offset} 0x6400000;\
      nand write 0x10000000 ${app_offset} ${temp_file_size};\
   else \
      echo unzip app.img.gz error !;\
    showMessage "unzip app.img.gz failed";exit_from_shell;\
   fi; \
else \
    echo tar app.img.gz error !;\
  showMessage "tar app.img.gz failed";exit_from_shell;\
fi;
#env
setenv temp_file_size;
setenv model   'MSO7014'
setenv softver '00.01.00.03.04'
setenv builddate '2017.03.25 17:32:52'
setenv bootlogo loadlogo

setenv nandboot 'loadzynq;run bootlogo;nand read 0x3000000 0x1100000 0xd966ca;bootm 0x3000000'
saveenv;

#update bootloader
if tar 0x4000000 0xA000000 boot.bin.gz; then \
   if unzip 0xA000000 0x10000000; then \
      storage qspi; \
      if sf probe; then \
         sf update 0x10000000 0 ${temp_file_size};\
      else \
         echo cannot find qspi \
       showMessage "sf boot.bin.gz failed";exit_from_shell;\
      fi; \
   else \
      echo uzip boot.bin.gz error !;\
    showMessage "unzip boot.bin.gz failed";exit_from_shell;\
   fi; \
else \
    echo tar boot.bin.gz error !;\
  showMessage "tar boot.bin.gz failed";exit_from_shell;\
fi;
##end##
