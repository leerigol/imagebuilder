#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include "cdpu.h"

CDpu::CDpu()
{
    m_fd = -1;
    m_pLayers = NULL;
    m_layerNum = 0;
}

CDpu::~CDpu()
{
    if ( m_pLayers != NULL && m_layerNum != 0 )
    {
        delete [] m_pLayers;
        m_pLayers = NULL;
        m_layerNum = 0;
    }
}

int CDpu::initialize( int fd )
{

    setFd( fd );
    setLayerNum( DPU_Layer_All );
    /// base
    m_pLayers[DPU_Layer_Back].setWidthHeight( SCREEN_WIDTH, SCREEN_HEIGHT );
    m_pLayers[DPU_Layer_Back].memmap(fd);
    ///wav
    m_pLayers[DPU_Layer_Wave].setWidthHeight( WAVE_WIDTH, WAVE_HEIGHT );
    m_pLayers[DPU_Layer_Wave].setColorDepth(4);
    m_pLayers[DPU_Layer_Wave].memmap(fd);

    ///wav
//    m_pLayers[DPU_Layer_Eye].setWidthHeight( WAVE_WIDTH, WAVE_HEIGHT );
//    m_pLayers[DPU_Layer_Eye].memmap(fd);

    /// fore
    m_pLayers[DPU_Layer_Fore].setWidthHeight( SCREEN_WIDTH, SCREEN_HEIGHT );
    m_pLayers[DPU_Layer_Fore].memmap(fd);

    return 0;
}

int CDpu::setLayerNum( int num )
{
    int i;

    if ( m_layerNum !=  num )
    {
        if ( m_pLayers != NULL )
        {
            delete [] m_pLayers;
        }

        m_pLayers = new  CLayer[num];

        if ( NULL == m_pLayers )
        {
            return -1;
        }

        for ( i = 0; i < num; i++ )
        {
            m_pLayers[i].setId( i );
        }

        m_layerNum = num;

    }
    else //if ( m_layerNum == num )
    {

    }

    return 0;
}


int CDpu::setFd( int fd )
{
    if ( fd == -1 )
    {
        return -1;
    }

    m_fd = fd;
    return 0;
}

int CDpu::memmap()
{
    if ( m_fd == -1 || m_pLayers == NULL )
    {
        return -1;
    }

    for ( int i = 0; i < m_layerNum; i++ )
    {
        if ( 0 != m_pLayers[i].memmap( m_fd ) )
        {
            return -2;
        }

    }

    return 0;
}

CLayer & CDpu::operator[](int id )
{
    if ( id < 0 || id >= m_layerNum )
    {
        return ( m_pLayers[0] );
    }

    return ( m_pLayers[id]);
}
