#ifndef CDPU_H
#define CDPU_H

#include "clayer.h"

#define SCREEN_WIDTH    	1024
#define SCREEN_HEIGHT   	600

#define WAVE_WIDTH			1000
#define WAVE_HEIGHT			480
enum
{
    DPU_Layer_Back,
    DPU_Layer_Wave,
    DPU_Layer_Eye,
    DPU_Layer_Fore,
    DPU_Layer_Comp,
    DPU_Layer_All,
};

class CDpu
{
public:
    CDpu();
    ~CDpu();

public:
    int initialize( int fd );
    int setLayerNum( int num );
    int setScreen( int width, int height );

    int setFd( int fd );
    int memmap();

public:
    CLayer & operator[](int id);
private:
    int m_fd;
    int m_layerNum;
    int m_screenWidth;
    int m_screenHeight;
protected:
    CLayer * m_pLayers;

};

#endif // CDPU_H
