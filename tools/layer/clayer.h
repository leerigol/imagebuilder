#ifndef CLAYER_H
#define CLAYER_H

enum
{
    // set layer index,
        DPU_SET_LAYER_ID   =	0x0F000000,
        DPU_GET_LAYER_ID   ,

    // set trace
        DPU_SET_TRACE_MASK      ,
        DPU_SET_TRACE_COLOR     ,
        DPU_SET_TRACE_DATA      ,
        DPU_SET_TRACE_VECTOR    ,

        DPU_SET_LAYER_RST 		,
        DPU_SET_LAYER_ALPHA		,
        DPU_SET_LAYER_OPEN		,
        //SET the x and y position of wave layer
        DPU_SET_WAVE_XY			,
        DPU_SET_LAYER_CLOSE		,
        DPU_SET_HDMI			,
        DPU_SCR_PRINT			,
        DPU_SEL_SIG
};


class CLayer
{
public:
    CLayer();
    CLayer(int index);

    ~CLayer();

public:
    int setId( int id );
    int setWidthHeight( int wid, int height );
    int setColorDepth(int d) {m_colorDepth = d; return 0;}
    void *getAddr();

public:
    int drawRect( int x, int y, int wid, int height, unsigned int color );
    int drawImage( int x, int y, unsigned char *pDat, int wid, int het, int colorDepth = 4);
    //int drawImage(const QRect & target, uchar* pDat, const QRect & source, int depth = 4);
    int clear( unsigned char *pColor, int depth = 2 );
    int clear( unsigned int color ,int depth = 2);

public:    
    int memmap( int fd );


    void *m_addr;
    int   m_index;

    int   m_width;
    int   m_height;
    int   m_colorDepth;
};

#endif // CLAYER_H
