#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

#include "clayer.h"

//
CLayer::CLayer()
{
    m_index = 0;
    m_addr = NULL;

    m_colorDepth = 2;
}

CLayer::CLayer(int index)
{
    m_index = index;
}

CLayer::~CLayer()
{
    if ( m_addr )
    {
        munmap( m_addr, m_width * m_height * m_colorDepth );
    }
}

int CLayer::setId(int index)
{
    m_index = index;
    return 0;
}


int CLayer::setWidthHeight( int wid, int height )
{
    m_width = wid;
    m_height = height;

    return 0;
}
void *CLayer::getAddr()
{
    return m_addr;
}

int CLayer::drawRect( int x, int y, int wid, int height, unsigned int color )
{
    int i, j;
    unsigned char *pSrc;

    pSrc = (unsigned char*)m_addr + x * m_colorDepth + y * m_width * m_colorDepth;
    for ( i = 0; i < height && i < (m_height - y); i++ )
    {
        for( j = 0; j < wid && j < (m_width - x ); j++ )
        {
            memcpy( pSrc + j * m_colorDepth, &color, m_colorDepth );
        }

        pSrc += m_width * m_colorDepth;
    }

    return 0;
}
//int CLayer::drawImage(const QRect & target, uchar *pDat, const QRect & source, int depth)
//{
//    uchar *pSrc = pDat + source.y() * m_width * depth + source.x() * depth ;

//    uchar *pDst = (uchar*)m_addr +  target.y() * m_width * depth + target.x() * depth;

//	int i,j;
//	for(i=0; i<source.height(); i++)
//	{
//		for(j=0; j<source.width(); j++)
//		{
//			memcpy( pDst + j * depth, pSrc + j * depth, depth );
//		}
//        pDst += m_width * depth;
//        pSrc += m_width * depth;
//	}

//	return 0;
//}
int CLayer::drawImage( int x, int y, unsigned char *pDat, int wid, int het, int colorDepth)
{
    unsigned char *pSrc, *pDst;
    int i, j;

      pSrc = pDat;
    pDst = (unsigned char*)m_addr + y * m_width * colorDepth + x * colorDepth;

    for ( i = 0; i < het && i < (m_height - y); i++ )
    {
        for ( j = 0; j < wid && j < (m_width - x); j++ )
        {
            memcpy( pDst + j * colorDepth, pSrc + j * colorDepth, colorDepth );
        }
        pDst += m_width * m_colorDepth;
        pSrc += wid * colorDepth;
    }

    return 0;
}

int CLayer::clear( unsigned char *pColor, int depth )
{
    int i, j;
    unsigned char *pDst;

    if ( NULL == m_addr )
    {
        return -1;
    }

    pDst = (unsigned char*)m_addr;
    for ( i = 0; i < m_height; i++ )
    {
        for ( j = 0; j < m_width; j++ )
        {
            memcpy( pDst + j * depth, pColor, depth );
        }

        pDst += m_width * depth;
    }
    printf("width=%d,height=%d\n", m_width, m_height);
    return 0;
}

int CLayer::clear( unsigned int color, int depth )
{
    return clear( (unsigned char*)&color, depth );
}


int CLayer::memmap( int fd )
{
    char *buf;


    ioctl(fd, DPU_SET_LAYER_ID, m_index);

    buf = (char *)mmap(0, m_width * m_height * m_colorDepth ,
                           PROT_READ | PROT_WRITE, MAP_SHARED,
                                     fd, 0);

    if ( NULL == buf )
    {
        return -1;
    }

    m_addr = buf;

    return 0;
}
