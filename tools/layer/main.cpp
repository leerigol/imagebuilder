#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include "cdpu.h"


#if 0

int main(int argc, char * argv[] )
{
    int fd = open("/dev/fb0", O_RDWR);
    if(fd == -1)
    {
      printf("Cant' open /dev/fb0\n");
      return -1;
    }

    if(argc == 2 && strncmp(argv[1], "-v", 2) == 0)
    {
        printf("Version=%s,%s", __DATE__, __TIME__);
        return 0;
    }
    ioctl(fd, DPU_SET_LAYER_RST,0);

    close(fd);
    return 0;
}
#else
int main(int , char **  )
{
	CDpu DPU;

    int fd = open("/dev/fb0", O_RDWR);
    if(fd == -1)
    {
      printf("Cant' open /dev/fb0\n");
      return -1;
    }


    DPU.initialize(fd);


//    unsigned int color = 0;
//    DPU[DPU_Layer_Back].clear( color );

//    //DPU[DPU_Layer_Eye].clear(0xcccccccc);

//    color = 0xff00ff00;
//    DPU[DPU_Layer_Wave].clear(color);

//    color = 0;
//    DPU[DPU_Layer_Eye].clear(color);
//    DPU[DPU_Layer_Fore].clear(color);


    ioctl(fd, DPU_SET_LAYER_OPEN, DPU_Layer_Fore);

    ioctl(fd, DPU_SET_LAYER_ALPHA, 0);

#if 1
    unsigned  *p = (unsigned *)DPU[DPU_Layer_Wave].m_addr;

    char path[] = "./wav.hex";
    int file_fd = open(path, O_WRONLY|O_CREAT);
    printf("Get Wave Layer Data...%s\n", path);
    if( file_fd )
    {
        for(int i=0; i<480; i++)
        {
            write(file_fd, p,1000*4);
            p += 1000;
        }
        printf("OK");

        close(file_fd);
    }
    else
    {
        printf("open fail\n");
    }

#endif
    close(fd);
    return 0;
}
#endif
