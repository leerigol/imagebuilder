#include <QCoreApplication>
#include <QtNetwork>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    if(argc < 2)
    {
        qDebug()<<"Please enter the scpi command";
        return a.exec();
    }
    QString cmdStr        = QString("%1\n").arg(argv[1]);

#if 0 //!用本机ip
    //!**获取本机IP**/
    QString hostIpaddress = "";
    QList<QHostAddress> NetList = QNetworkInterface::allAddresses(); //取得全部信息
    for(int i = 0; i< NetList.count(); i++)
    {
        if (    (NetList.at(i) != QHostAddress::LocalHost)
                && (NetList.at(i).toIPv4Address())
                )
        {
            hostIpaddress = NetList.at(i).toString();
            //qDebug()<<i<<"-----"<<hostIpaddress;
            break;
        }
    }

    //!**发送数据**/
    if( !hostIpaddress.isEmpty() )
    {
        QTcpSocket client;
        client.connectToHost(QHostAddress(hostIpaddress), 5566);
        client.waitForConnected();

        client.write(cmdStr.toLatin1().data());
        client.waitForBytesWritten();
    }
    else
    {
        qDebug()<<"hostIpaddress is null!";
        return a.exec();
    }
#else //!用本地回环ip
    QTcpSocket client;
    client.connectToHost( "127.0.0.1", 5555);
    if(client.waitForConnected())
    {
//        qDebug()<<"Network connection ok!";
        client.write(cmdStr.toLatin1().data());
        client.waitForBytesWritten();
    }
    else
    {
        qDebug()<<"Network connection failure!";
        return a.exec();
    }
#endif
    return 0;
}

