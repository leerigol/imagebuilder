#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#define SIZE 256
#define I2C_FILE_NAME "/dev/i2c-0"
#define I2C_ADDR 0x48

struct ChipSetting {
	char No;
	char Reg;
	char Data1;
	char Data2;
};

// SSD2533 Setting
// Touch Panel Example
struct ChipSetting ssd254xcfgTable[]={							
//Modified by SSL on 20170504
{2,0x06,0x15,0x25}, 
{2,0x28,0x00,0x04}, 
{2,0x07,0x00,0xe6}, 
{2,0x08,0x00,0xe7}, 
{2,0x09,0x00,0xe8}, 
{2,0x0a,0x00,0xe9}, 
{2,0x0b,0x00,0xea}, 
{2,0x0c,0x00,0xeb}, 
{2,0x0d,0x00,0xec}, 
{2,0x0e,0x00,0xed}, 
{2,0x0f,0x00,0xee}, 
{2,0x10,0x00,0xef}, 
{2,0x11,0x00,0xf0}, 
{2,0x12,0x00,0xf1}, 
{2,0x13,0x00,0xf2}, 
{2,0x14,0x00,0xf3}, 
{2,0x15,0x00,0xf4}, 
{2,0x16,0x00,0xf5}, 
{2,0x17,0x00,0xf6}, 
{2,0x18,0x00,0xf7}, 
{2,0x19,0x00,0xf8}, 
{2,0x1a,0x00,0xf9}, 
{2,0x1b,0x00,0xfa}, 
{2,0x1c,0x00,0xfb}, 
{2,0xd4,0x00,0x08},//{2,0xd4,0x00,0x0d} 
{2,0xd7,0x00,0x04}, 
{2,0xd8,0x00,0x01}, 
{2,0xdb,0x00,0x05}, 
{2,0x30,0x02,0x0F}, 
{2, 0x33, 0x00, 0x03},
{2, 0x34, 0xc6, 0xC8}, //0511
{2,0x36,0x00,0x1a}, 
{2,0x37,0x07,0xC4}, 
{2,0x38,0x00,0x01}, 
{2,0x2B,0x17,0xD4}, 
{2, 0x40, 0x10, 0xC8},
{2, 0x41, 0x00, 0x30},
{2, 0x42, 0x00, 0x20},//50
{2, 0x43, 0x00, 0x20},
{2, 0x44, 0x00, 0x20},//50
{2, 0x45, 0x00, 0x00},
{2, 0x46, 0x10, 0x1F},
{2,0x56,0x80,0x10}, 
{2,0x59,0x80,0x10}, 
{2,0x65,0x00,0x02}, 
{2,0x66,0x1a,0xf4}, 
{2,0x67,0x1b,0x44}, 
{2,0x7a,0xff,0xff}, 
{2,0x7b,0x00,0x03}, 
{2,0x25,0x00,0x0c},
};

struct ChipSetting Reset[]={
    { 2, 0x04, 0x00, 0x00},     // SSD254X
};


static int set_i2c_register(int file,  
                            unsigned short addr,  
                            unsigned short reg,  
                            unsigned char *value) {  
  
    unsigned char outbuf[SIZE+2];
	int i=0;

    struct i2c_rdwr_ioctl_data packets;  
    struct i2c_msg messages[1];  
  
    messages[0].addr  = addr;  
    messages[0].flags = 0;  
    messages[0].len   = 3;  
    messages[0].buf   = outbuf;  
  
    
    outbuf[0] = reg & 0xff;
    outbuf[1] = *(value+1);
	outbuf[2] = *(value);
	
    /* Transfer the i2c packets to the kernel and verify it worked */  
    packets.msgs  = messages;  
    packets.nmsgs = 1;  
    if(ioctl(file, I2C_RDWR, &packets) < 0) {  
        perror("Unable to send data");  
        return 1;  
    }  
  
    return 0;  
}  

void resetTouch( int i2c_file)
{
	int i;
	char value[4];
	for(i=0;i<sizeof(Reset)/sizeof(Reset[0]);i++)
	{
		value[0] = Reset[i].Data1;
		value[1] = Reset[i].Data2;
		if( 0 == set_i2c_register(i2c_file, I2C_ADDR, Reset[i].Reg, (unsigned char*)value) )
		{
			;
		}
		else
		{
			printf("Reset Sending error\n");
			break;
		}
	}
}


void main(void)
{

	int i2c_file;
	int i;
	char value[4];

	if ((i2c_file = open(I2C_FILE_NAME, O_RDWR)) < 0) 
	{  
        perror("Unable to open i2c control file");  
        exit(1);  
    }


	for(i=0;i<sizeof(ssd254xcfgTable)/sizeof(ssd254xcfgTable[0]);i++)
	{
		value[0] = ssd254xcfgTable[i].Data1;
		value[1] = ssd254xcfgTable[i].Data2;
		if( 0 == set_i2c_register(i2c_file, I2C_ADDR, ssd254xcfgTable[i].Reg, (unsigned char*)value) )
		{
			;
		}
		else
		{
			printf("Sending error\n");
			break;
		}
	}
	
	close(i2c_file);
}

