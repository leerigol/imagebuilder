#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/spi/spidev.h>
#include "../spi/spi_bus.h"

#define BUS_SIZE (64*1024)
#define BUS_NAME "/dev/mem"
#define GP1_BASE 0x83C00000
#define GP0_BASE 0x43C00000

#define SPI_NAME  	"/dev/spidev0.0"
#define SPI_MODE	0
#define SPI_BITS	8
#define SPI_RATE	10000000
#define SPI_DELY	0

static void pabort(const char *s)
{
      perror(s);
      abort();
}

static int transfer(int fd, unsigned char *ts_buf, unsigned int len)
{
	int rx = 0;
	int ret;
	struct spi_ioc_transfer tr = {
             .tx_buf = (unsigned long)ts_buf,   //定义发送缓冲区指针
             .rx_buf = (unsigned long)&rx,   //定义接收缓冲区指针
             .len = len,
             .delay_usecs = SPI_DELY};

    	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);//执行spidev.c中ioctl的default进行数据传输

	if (ret == 1)
	{
        printf("xx.xx.spi\n");
		exit(1);
	}

	rx  =  rx >> 8;
	ret =  rx & 0xff;
	rx  =  rx >> 8;
	rx  =  rx | (ret << 8);
	return rx;	
}


unsigned long simple_strtol(const char *cp, char **endp,unsigned int base)
{
	unsigned long result = 0;
	unsigned long value;

	if (*cp == '0') {
		cp++;
		if ((*cp == 'x') && isxdigit(cp[1])) {
			base = 16;
			cp++;
		}

		if (!base)
			base = 8;
	}

	if (!base)
		base = 10;

	while (isxdigit(*cp) && (value = isdigit(*cp) ? *cp-'0' : (islower(*cp)
	    ? toupper(*cp) : *cp)-'A'+10) < base) {
		result = result*base + value;
		cp++;
	}

	if (endp)
		*endp = (char *)cp;

	return result;
}

int read_from_cpld(unsigned char addr)
{
	unsigned char ts[] = {0x00,0x00,0x00};
	int fd = open(SPI_NAME, O_RDWR);       //打开"/dev/spidev1.0"
	int ret = 0;
	if (fd < 0)
	{
	    printf("xx.xx.xx\n");
		exit(1);
	}
	int cfg = SPI_MODE;
	ret = ioctl(fd, SPI_IOC_WR_MODE, &cfg);  //SPI模式设置可写
	if (ret == -1)
		 pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &cfg); //SPI模式设置可读
	if (ret == -1)
		 pabort("can't get spi mode");

	cfg = SPI_BITS;
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &cfg);  //SPI的bit/word设置可写
	if (ret == -1)
		 pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &cfg);   //SPI的bit/word设置可读
	if (ret == -1)
		 pabort("can't get bits per word");

	cfg = SPI_RATE;
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &cfg);     //SPI的波特率设置可写
	if (ret == -1)
		 pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &cfg);   //SPI的波特率设置可读
	if (ret == -1)
		 pabort("can't get max speed hz");

	SPI_SEL_CPLD();
	ts[0] = addr;
	//transfer(fd, ts, 3);
	ret = transfer(fd, ts, 3);	
	close(fd);
	return ret;
}
#if 1
int main(int argc, char*argv[])
{

	int tx = 0;
	int rx = 0;
	int offset = 0;
	int dev_mem = -1;
	unsigned int axi_bus_base = 0;

	if(argc < 2)
	{
		printf("BuildDate:%s\n", __DATE__);
		printf("read-ver address\n");
		exit(1);
	}

	offset = simple_strtol(argv[1], NULL , 10);
	if(argc == 2)
	{
		unsigned int base = 0;
		
		base   = offset & 0xffff0000;
		if(base) // read through AXI
		{
			dev_mem = open(BUS_NAME, O_RDWR);
			if(dev_mem == -1)
			{
				printf("xx.xx.xx\n");
				exit(1);
			}

			axi_bus_base = (unsigned int)mmap(NULL,BUS_SIZE,PROT_READ|PROT_WRITE,MAP_SHARED,dev_mem, base);	
		
			if(axi_bus_base == 0)
			{
				printf("xx.xx.xx\n");
				close(dev_mem);
				exit(1);
			}

			printf("%x\n",*(volatile unsigned int*)(axi_bus_base + (offset & 0xffff)));	
			munmap((void*)axi_bus_base, BUS_SIZE); //destroy map memory
			close(dev_mem);
		}
		else //cpld
		{
			int ver = read_from_cpld(offset & 0xff);
			printf("%x\n", ver);
		}
	}

	return 0;
}

#else

int main(int argc, char*argv[])
{

	int tx = 0;
	int rx = 0;
	int offset = 0;
	int fd = -1;
	
	if(argc < 2)
	{
		printf("read-ver offset\n");
		exit(1);
	}

	fd = open("/dev/axi", O_RDWR);

	if(fd == -1)
	{
		printf("Cant' open /dev/axi\n");
		exit(1);
	}

	if(argc == 2)
	{
		offset = simple_strtol(argv[1], NULL , 10);
		//printf("offset=0x%x\n",offset);
		ioctl(fd, 0, offset);
		if(read(fd, &rx,4) == 4)
		{
			printf("%x\n", rx);			
		}
		else
		{
			printf("xx.xx.xx\n");
		}
		close(fd);
	}
	return 0;
}
#endif
