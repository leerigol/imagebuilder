#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#define BUS_SIZE (64*1024)
#define BUS_NAME "/dev/mem"
#define BUS_BASE 0x83C00000

unsigned long simple_strtol(const char *cp, char **endp,unsigned int base)
{
	unsigned long result = 0;
	unsigned long value;

	if (*cp == '0') {
		cp++;
		if ((*cp == 'x') && isxdigit(cp[1])) {
			base = 16;
			cp++;
		}

		if (!base)
			base = 8;
	}

	if (!base)
		base = 10;

	while (isxdigit(*cp) && (value = isdigit(*cp) ? *cp-'0' : (islower(*cp)
	    ? toupper(*cp) : *cp)-'A'+10) < base) {
		result = result*base + value;
		cp++;
	}

	if (endp)
		*endp = (char *)cp;

	return result;
}
#if 0
int main(int argc, char*argv[])
{

	int tx = 0;
	int rx = 0;
	int offset = 0;
	int dev_mem = -1;
	unsigned int axi_bus_base = 0;

	if(argc < 2)
	{
		printf("BuildDate:%s\n", __DATE__);
		printf("axi offset [value]\n");
		exit(1);
	}

	dev_mem = open(BUS_NAME, O_RDWR);

	if(dev_mem == -1)
	{
		printf("Cant' open %s\n", BUS_NAME);
		exit(1);
	}

	axi_bus_base = (unsigned int)mmap(NULL,BUS_SIZE,PROT_READ|PROT_WRITE,MAP_SHARED,dev_mem, BUS_BASE);
	if(axi_bus_base == 0)
	{
		printf("Cant'n map 0x%x\n", BUS_BASE);
		close(dev_mem);
		exit(1);
	}

	if(argc == 2)
	{
		offset = simple_strtol(argv[1], NULL , 10);
		printf("Read(%s)=#0x%x#\n", argv[1], *(volatile unsigned int*)(axi_bus_base + offset));			
	}
	else
	{
		offset 	=	simple_strtol(argv[1], NULL , 10);
		tx		=	simple_strtol(argv[2], NULL , 10);

		*(volatile unsigned int*)(axi_bus_base + offset) = (unsigned int)tx;
		printf("Write:OK\n");
	}

	munmap((void*)axi_bus_base, BUS_SIZE); //destroy map memory
	close(dev_mem);
	return 0;
}

#else

int main(int argc, char*argv[])
{

	int tx = 0;
	int rx = 0;
	int offset = 0;
	int fd = -1;
	
	if(argc < 2)
	{
		printf("axi offset [value]\n");
		exit(1);
	}

	fd = open("/dev/axi", O_RDWR);

	if(fd == -1)
	{
		printf("Cant' open /dev/axi\n");
		exit(1);
	}

	if(argc == 2)
	{
		int i;
		offset = simple_strtol(argv[1], NULL , 10);
		printf("offset=0x%x\n",offset);
		ioctl(fd, 0, offset);

		for(i=0; i<100000; i++ )
		{
			read(fd, &rx,4);
			if(rx == 0xeeeeeeee)
			{
				printf("Error on %d\n", i);
			}
			else
			{
				//printf("Read #Error#\n");
			}
			//sleep(1);
		}
		close(fd);
	}
	else
	{
		offset 	=	simple_strtol(argv[1], NULL , 10);
		tx		=	simple_strtol(argv[2], NULL , 10);
		//printf("offset=0x%x, value=0x%x\n",offset,tx);
		ioctl(fd, 0, offset);
		if(write(fd, &tx, 4) == 4)
		{
			printf("Write:OK\n");
		}
		else
		{
			printf("Write:Error\n");			
		}
		close(fd);
	}

	return 0;
}
#endif
