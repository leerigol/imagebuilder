if(!(cont) ){  \
  14     int  nptrs; \
  15     void *buffer[100]; \
  16     nptrs = backtrace(buffer, 100);\
  17     char** strings = backtrace_symbols(buffer, nptrs);\
  18     if (strings ) \
  19     {\
  20         QString file_name = QString("/user/data/bug_%1_%2_%3.sh").arg(__FILE__).arg(__FUNCTION__).arg(__LINE__);\
  21         QFile file(file_name); \
  22         if(file.open(QIODevice::WriteOnly | QIODevice::Text)) \
  23         { \
  24             QTextStream out( &file );\
  25             out << "#!/bin/bash\n\n";\
  26             out << "result=/tmp/result" << "\n";\
  27             for (int j = 0; j < nptrs; j++)\
  28             {\
  29                 QString tmp = QString(strings[j]);\
  30                 if( tmp.startsWith("flam"))\
  31                 {\
  32                     int pos = tmp.indexOf('[') + 1;\
  33                     out << "arm-xilinx-linux-gnueabi-addr2line -e flamingo_console "\
  34                         << tmp.mid(pos, tmp.size()-pos - 1)\
  35                         << " >> $result\n";\
  36                 }\
  37             }\
  38             out << "cat $result\n" ;\
  39             fsync(file.handle());\
  40             file.close();\
  41             free(strings);\
  42         }\
  43     }\
  44     else \
  45     { qDebug() << "R_ASSERT error.";}\
  46     Q_ASSERT(false);\
  47 }
