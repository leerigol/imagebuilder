#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "mb85rc.h"

Xuint8 rbuf[1] = {0x0}; 
Xuint8 wbuf[] = {0xaa};

Xuint8 printarray(Xuint8 Array[], int Num)
{
	int i;
  
  	for(i=0;i<Num;i++)
  	{
		if(i%25 == 0)
			printf("\n");
    		printf("Data [%d] is %x \n", i ,Array[i]);
  	}
  
  	return(1);
}

#define VALUE 0x5A
int main()
{
	int i;
	unsigned char buf[PAGESIZE];

	MB85RC_Init();

	for(i=0; i<PAGESIZE; i++)
	{
		buf[i] = VALUE;
	}
	printf("Writing Data [0-8KB]\n");
	MB85RC_Write(0, buf, sizeof(buf) );
	//MB85RC_Write(250, buf, sizeof(buf) );
	
	printf("Reading Data [0-8KB]\n");
	memset(buf, 0, sizeof(buf));

	MB85RC_Read(0,buf,PAGESIZE);
	for(i=0; i<PAGESIZE; i++)
	{
		if(buf[i] != VALUE)
		{
			printf("Read Error:%d, 0x%x\n", i, buf[i]);
		}
		else
		{
			printf("Read OK:%d, 0x%x\n", i, buf[i]);
		}
	}
	MB85RC_Close();
}
