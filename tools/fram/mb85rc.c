#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include "mb85rc.h"

int MB85RC_Init(void)
{
	Fdiic = open("/dev/i2c-0", O_RDWR);   //打开设备并，允许读写
  
	if(Fdiic < 0)
	{
		perror("Can't open /dev/i2c-0 \n"); 
		exit(1);
	}
	else
	{
		perror("Open /dev/i2c-0 \n");
	}
	
	return(1);
}

void MB85RC_Close(void)
{
	close(Fdiic);
}

static Xuint8 WriteData(Xuint8 WriteBuffer[], Xuint8 NoOfBytes)
{
	Xuint8 BytesWritten;    /* Number of Bytes written to the IIC device. */
	
	/*
	 * Write the bytes onto the bus.
	 */
	BytesWritten = write(Fdiic, WriteBuffer, NoOfBytes);
	 
	/*
	 * Wait till the EEPROM internally completes the write cycle.
	 */
	
	return BytesWritten;
}

int MB85RC_Write(Xuint16 OffsetAddr, Xuint8 *data, Xuint16 BytesToWrite)
{
	Xuint8 WriteBuffer[PAGESIZE + sizeof(AddressType)]; /* Buffer to hold data to be written */
	Xuint16 BytesWritten;   /* Number of Bytes written to the IIC device. */
	Xuint16 Index;                          /* Loop variable. */
	Xuint16 OffsetAddrNew = 0x0;
	Xuint16 WriteBytes;
	Xuint8 Data = 0x0;
	char Input[8];
	int Status = 0;
	int i;

	Status = ioctl(Fdiic, I2C_SLAVE_FORCE, EEPROM_ADDR);
	if(Status < 0)
	{
		printf("\n Unable to set the EEPROM address\n");
		return -1;
	}
	
	/*
	 * Load the offset address inside EEPROM where data need to be written.
	 */
	if(sizeof(AddressType) == 1)
	{
		WriteBuffer[0] = (Xuint8)(OffsetAddr);
	}
	else
	{
		WriteBuffer[0] = (Xuint8)(OffsetAddr >> 8);
		WriteBuffer[1] = (Xuint8)(OffsetAddr);
	}

	/*
	 *Load the data to be written into the buffer.
	 */
	for(Index = 0; Index < PAGESIZE; Index++)
	{
		WriteBuffer[Index + sizeof(AddressType)] = data[Index];
	}
	 
	/*
	 * Limit the number of bytes to the page size.
	 */
	if(BytesToWrite > PAGESIZE)
	{
		WriteBytes = PAGESIZE + sizeof(AddressType);
	}
	else
	{
		WriteBytes = BytesToWrite + sizeof(AddressType);
	}
	 
	while(BytesToWrite > 0)
	{
		/*
		 * Write the data.
		 */
		BytesWritten = WriteData(WriteBuffer, WriteBytes);
		
		if(BytesWritten != WriteBytes)
		{
			printf("\nTest Failed in write\n");
			return -1;
		}
		 
		/*
		 * Update the write counter.
		 */
		BytesToWrite -= (BytesWritten - sizeof(AddressType));
		if(BytesToWrite > PAGESIZE)
		{
			WriteBytes = PAGESIZE + sizeof(AddressType);
		}
		else
		{
			WriteBytes = BytesToWrite + sizeof(AddressType);
		}

		/*
		 * Increment the offset address.
		 */
		OffsetAddr += PAGESIZE;
		
		/*
		 * Increment the data.
		 */
		Data++;
		
		/*
		 * load the new offset address.
		 */
		if(sizeof(AddressType) == 1)
		{
			WriteBuffer[0] = (Xuint8)(OffsetAddr);
		}
		else
		{
			WriteBuffer[0] = (Xuint8)(OffsetAddr >> 8);
			WriteBuffer[1] = (Xuint8)(OffsetAddr);
		}
		 
		/*
		 * load the new data into buffer.
		 */
		for(Index = 0; Index < PAGESIZE; Index++)
		{
			WriteBuffer[Index + sizeof(AddressType)] = Data;
		}
	}
	 
	return 0;
}

static Xuint8 ReadData(Xuint8 ReadBuffer[], Xuint8 NoOfBytes)
{
	Xuint8 BytesRead;       /* Number of Bytes read from the device. */
	 
	/*
	 * Read the bytes from the bus.
	 */
	BytesRead = read(Fdiic, ReadBuffer, NoOfBytes);
	 
	return BytesRead;
}

int MB85RC_Read(Xuint16 OffsetAddr,Xuint8 *data, Xuint16 BytesToRead)
{
	Xuint8 WriteBuffer [2]; /* Buffer to hold location address.*/
	Xuint8 BytesWritten;
	Xuint16 BytesRead=0;                    /* Number of Bytes read from the IIC device. */
	Xuint16 Index;                          /* Loop variable. */
	Xuint16 ReadBytes;
	int Status = 0;
	int i;

	Status = ioctl(Fdiic, I2C_SLAVE_FORCE, EEPROM_ADDR);
	if(Status < 0)
	{
		printf("\n Unable to set the EEPROM address\n");
		return -1;
	}
	 
	if(sizeof(AddressType) == 1)
	{
		WriteBuffer[0] = (Xuint8)(OffsetAddr);
	}
	else
	{
		WriteBuffer[0] = (Xuint8)(OffsetAddr >> 8);
		WriteBuffer[1] = (Xuint8)(OffsetAddr);
	}

	/*
	 * Position the address pointer in EEPROM.
	 */
	BytesWritten = write(Fdiic, WriteBuffer, sizeof(AddressType));
	
	/*
	 * limit the bytes to be read to the Page size.
	 */
	if(BytesToRead > PAGESIZE)
	{
		ReadBytes = PAGESIZE;
	}
	else
	{
		ReadBytes = BytesToRead;
	}
	 
	/*
	 * Read the bytes.
	 */
	while(BytesToRead > 0)
	{
		BytesRead = ReadData(data, ReadBytes);
		
		if(BytesRead != ReadBytes)
		{
			printf("\nITP_IIC_TEST1: Test Failed in read\n");
			return -1;
		}
		 
		/*
		 * Update the read counter.
		 */
		BytesToRead -= BytesRead;
		if(BytesToRead > PAGESIZE)
		{
			ReadBytes = PAGESIZE;
		}
		else
		{
			ReadBytes = BytesToRead;
		}
		
	}
	 
	return 0;
}

