#ifndef MB85RC__H
#define MB85RC__H
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

/**************************** Type Definitions *******************************/
typedef unsigned char   Xuint8;
typedef char            Xint8;      /**< signed 8-bit */
typedef unsigned short  Xuint16;    /**< unsigned 16-bit */
typedef short           Xint16;     /**< signed 16-bit */
typedef unsigned long   Xuint32;    /**< unsigned 32-bit */
typedef long            Xint32;     /**< signed 32-bit */
typedef float           Xfloat32;   /**< 32-bit floating point */
typedef double          Xfloat64;   /**< 64-bit double precision floating point */
typedef unsigned long   Xboolean;   /**< boolean (XTRUE or XFALSE) */
typedef  Xuint16 AddressType;

//I2C总线模式设置
#define I2C_RETRIES     0x0701
#define I2C_TIMEOUT     0x0702
#define I2C_SLAVE_FORCE 0x0706
#define I2C_SLAVE	0x0703	/* Change slave address			*/
#define I2C_FUNCS	0x0705	/* Get the adapter functionality */
#define I2C_RDWR	0x0707	/* Combined R/W transfer (one stop only)*/

/*****************************************slave addr************************************/
#define EEPROM_ADDR 	0x52

/*
 * Page size of the FRAM. This depends on the type of the FRAM available
 * on board.
 */
#define PAGESIZE        254

//I2C设备端口号
int Fdiic;

/* I2C总线ioctl方法所使用的结构体 */
struct i2c_rdwr_ioctl_data rdwrdata;

/***********************************************************
 * 功能：初始化FRAM，打开I2C设备，设置总线重发次数和超时限制，
 * 返回值：无
 ***********************************************************/
int MB85RC_Init(void);

/************************************************************
 * 功能：从mb85rc的pos位置处读取数据
 * 参数：
 * 	@OffsetAddr: FRAM的Read address,mb85rc64的read address
 * 	是由High 8 bits和Low 8 bits共16位组成的。
 * 	@data:从FRAM读出的数据
 * 	@BytesToRead：读出字节的个数
 * 返回值：读取的字节数据个数 
 ************************************************************/
int MB85RC_Read(Xuint16 OffsetAddr, Xuint8 *data, Xuint16 BytesToRead);

/************************************************************
 * 功能：将数据写入eeprom的pos位置
 * 参数：
 * 	@OffsetAddr: FRAM的Write address,mb85rc64的write address
 * 	是由High 8 bits和Low 8 bits共16位组成的。
 *    	@data: 待写入FRAM的数据 
 *    	@BytesToWrite:待写入的字节数
 * 返回值：写入的字节数据个数   
 ************************************************************/
int MB85RC_Write(Xuint16 OffsetAddr, Xuint8 *data, Xuint16 BytesToWrite);

/************************************************************
 * 功能：关闭设备端口
 * 返回值:无 
 ************************************************************/
void MB85RC_Close(void);

#endif
