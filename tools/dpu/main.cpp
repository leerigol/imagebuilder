#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
enum
{
// set layer index,
    DPU_SET_LAYER_ID   =	0x0F000000,
    DPU_GET_LAYER_ID   ,

// set trace
    DPU_SET_TRACE_MASK         ,
    DPU_SET_TRACE_COLOR        ,
    DPU_SET_TRACE_DATA         ,
    DPU_SET_TRACE_VECTOR       ,

    DPU_SET_LAYER_APPLY         ,
    DPU_SET_LAYER_DIS
};

#define CUR_TRACE 0x1

int main(int argc, char * argv[] )
{
    unsigned char trace_data[1001];
    int fd = open("/dev/fb0", O_RDWR);
    if(fd == -1)
    {
      printf("Cant' open /dev/fb0\n");
      return -1;
    }
 
    unsigned int trace_mask = (CUR_TRACE << 16 ) | 1;
    ioctl(fd, DPU_SET_TRACE_MASK, trace_mask);

    unsigned int trace_color= (CUR_TRACE << 16) | 0xff00;
    ioctl(fd, DPU_SET_TRACE_COLOR, trace_color);

    for(int i=0; i<1000; i++)
    {
        trace_data[i+1] = i & 0xff;
    }
    trace_data[0] = CUR_TRACE;
    ioctl(fd, DPU_SET_TRACE_DATA, &trace_data[0]);


    close(fd);
    return 0;
}
