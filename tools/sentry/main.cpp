#include <QLocalSocket>

int main(int argc, char **argv)
{
    //qDebug() << "sentry to" << argv[0];
    if(argc == 2 )
    {
        QLocalSocket ls;
        ls.connectToServer("/var/run/wpa_supplicant");

        if (ls.waitForConnected())
        {
                QTextStream ts(&ls);
                ts << argv[1];
                ts.flush();
                ls.waitForBytesWritten();
                //sleep(1);
        }
        else
        {
            qDebug() << "can't connect to the server";
        }
    }
    return 0;
}
