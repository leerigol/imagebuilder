#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "spi_bus.h"

 
static void pabort(const char *s)
{
      perror(s);
      abort();
}


static int transfer(int fd, unsigned char *ts_buf, unsigned int len)
{
	int rx = 0;
	int ret;
	struct spi_ioc_transfer tr = {
             .tx_buf = (unsigned long)ts_buf,   //定义发送缓冲区指针
             .rx_buf = (unsigned long)&rx,   //定义接收缓冲区指针
             .len = len,
             .delay_usecs = SPI_DELY};

    	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);//执行spidev.c中ioctl的default进行数据传输

	if (ret == 1)
	{
       pabort("can't send spi message");
	}

	rx  =  rx >> 8;
	ret =  rx & 0xff;
	rx  =  rx >> 8;
	rx  =  rx | (ret << 8);
	return rx;	
}


unsigned long simple_strtol(const char *cp, char **endp,unsigned int base)
{
	unsigned long result = 0;
	unsigned long value;

	if (*cp == '0') {
		cp++;
		if ((*cp == 'x') && isxdigit(cp[1])) {
			base = 16;
			cp++;
		}

		if (!base)
			base = 8;
	}

	if (!base)
		base = 10;

	while (isxdigit(*cp) && (value = isdigit(*cp) ? *cp-'0' : (islower(*cp)
	    ? toupper(*cp) : *cp)-'A'+10) < base) {
		result = result*base + value;
		cp++;
	}

	if (endp)
		*endp = (char *)cp;

	return result;
}



int main(int argc, char *argv[])
{
	int ret = 0;
	int fd;
	int cfg;

	/* beeper on and backlight on*/
	unsigned char ts[] = {0x02,0x00,0x03};

	//printf("SPI-CPLD configuration\n");
	fd = open(SPI_NAME, O_RDWR);       //打开"/dev/spidev1.0"
	if (fd < 0)
	{
	  pabort("can't open device");
	}

	cfg = SPI_MODE;
	ret = ioctl(fd, SPI_IOC_WR_MODE, &cfg);  //SPI模式设置可写
	if (ret == -1)
		 pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &cfg); //SPI模式设置可读
	if (ret == -1)
		 pabort("can't get spi mode");

	cfg = SPI_BITS;
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &cfg);  //SPI的bit/word设置可写
	if (ret == -1)
		 pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &cfg);   //SPI的bit/word设置可读
	if (ret == -1)
		 pabort("can't get bits per word");

	cfg = SPI_RATE;
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &cfg);     //SPI的波特率设置可写
	if (ret == -1)
		 pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &cfg);   //SPI的波特率设置可读
	if (ret == -1)
		 pabort("can't get max speed hz");

	SPI_SEL_CPLD();

	if(argc < 2 )
	{
		 printf("spi2cpld cmd [para]----Build:%s\n", __DATE__);
		 
		 transfer(fd, ts, sizeof(ts));
		 ts[0] = 0x0;
		 ts[1] = 0;
		 ts[2] = 0;
		 transfer(fd, ts, sizeof(ts));
		 printf("CPLD Ver=#0x%x#\n", transfer(fd, ts, sizeof(ts)));
		 exit(1);
	}
	else
	{ 
		
		ts[0] = simple_strtol(argv[1],NULL, 10) & 0xff;

		if(argc == 3)
		{
			ret   = simple_strtol(argv[2],NULL, 10);
			ts[1] = (ret >> 8) & 0xff;
			ts[2] = ret & 0xff;
			transfer(fd, ts, sizeof(ts) );
			printf("cmd=0x%x, val=0x%x\n", ts[0], ret);
		}
		else
		{
			ts[1] = 0;
			ts[2] = 0;
			//transfer(fd, ts, 3);
			ret = transfer(fd, ts, 3);
			printf("Read(%s) = #0x%x#\n",argv[1], ret);
		}
	}

	close(fd);
	return ret;
}

