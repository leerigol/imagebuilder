#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "spi_bus.h"

static void pabort(const char *s)
{
      perror(s);
      abort();
}


static uint32_t BUFSZ = 4096;
static uint32_t BITSZ = 0x700000;

static int transfer(int fd, unsigned char *ts_buf, unsigned int len)
{
	int rx = 0;
	int ret;
	struct spi_ioc_transfer tr = {
             .tx_buf = (unsigned long)ts_buf,   //定义发送缓冲区指针
             .rx_buf = (unsigned long)&rx,   //定义接收缓冲区指针
             .len = len,
             .delay_usecs = SPI_DELY};

    ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);//执行spidev.c中ioctl的default进行数据传输

	if (ret == 1)
	{
        pabort("can't send spi message");
	}
	rx  =  rx >> 8;
	ret =  rx & 0xff;
	rx  =  rx >> 8;
	rx  =  rx | (ret << 8);
	return rx;
}


void set_spi_mode(int fd, uint8_t mode)
{
	int ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		 pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		 pabort("can't get spi mode");

}

void set_spi_bits(int fd, uint8_t bits)
{
	int ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		 pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		 pabort("can't get bits per word");
}

void set_spi_speed(int fd, uint32_t speed)
{
	int ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		 pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		 pabort("can't get max speed hz");	
}


int wait_for_done(int fd)
{
	unsigned char is_done = 0;
	unsigned char ts[] = {0x0d,0x00, 0x00};
	SPI_SEL_CPLD();
	do{
		is_done = transfer(fd, ts, 3);		
	}while( (is_done & 0x400) == 0 );
	printf("is_done=%x\n", is_done);
	printf("Program_Done is OK\n");

	return 0;
}


int main(int argc, char *argv[])
{
	int ret = 0;
	int fd;

	unsigned char* pk7buf = NULL;
	int   file_size = 0;
	

	if(argc < 2 )
	{
		pabort("spi2k7  k7-bit-file\n");
	}

	fd = open(SPI_NAME, O_RDWR);
	if (fd < 0)
	{
		pabort("can't open device");
	}

	set_spi_bits(fd, SPI_BITS);
	set_spi_mode(fd, SPI_MODE);
	set_spi_speed(fd, 24000000);
	printf("Preparing...\n");
		
	SPI_SEL_DEV();
	//sending k7 bit stream
	{
		FILE * file = fopen(argv[1], "r");
		if(file)
		{
			pk7buf = (unsigned char*)malloc(BITSZ);
			if(pk7buf)
			{
				int step;
				int i;
				int left_size;
				unsigned char* pbits = NULL;
				
				file_size = fread(pk7buf, 1, BITSZ, file);
				printf("Reading :0x%x\n", file_size);

				for(i=0; i<2; i++)
				{
					pbits = pk7buf;
					step = file_size / BUFSZ;
					printf("Sending...\n");	
					while(step)
					{
						write(fd, pbits, BUFSZ);
						pbits += BUFSZ;
						step--;
					}
					left_size = pk7buf + file_size - pbits;			
					write(fd, pbits, left_size);
				}
				free(pk7buf);
				//wait for done
				//wait_for_done(fd);
			}
			fclose(file);			
		}
	}

	close(fd);
	return ret;
}

