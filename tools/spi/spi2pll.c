#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "spi_bus.h"

static void pabort(const char *s)
{	
	printf(s);
	exit(1);
}


static int transfer(int fd, unsigned char *ts_buf, unsigned int len)
{
	int rx = 0;
	int ret;
	struct spi_ioc_transfer tr = {
             .tx_buf = (unsigned long)ts_buf,   //定义发送缓冲区指针
             .rx_buf = (unsigned long)&rx,   //定义接收缓冲区指针
             .len = len,
             .delay_usecs = SPI_DELY};

    ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);//执行spidev.c中ioctl的default进行数据传输

	if (ret == 1)
	{
        pabort("can't send spi message");
	}
	rx  =  rx >> 8;
	ret =  rx & 0xff;
	rx  =  rx >> 8;
	rx  =  rx | (ret << 8);
	return rx;
}


void set_spi_mode(int fd, uint8_t mode)
{
	int ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		 pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		 pabort("can't get spi mode");

}

void set_spi_bits(int fd, uint8_t bits)
{
	int ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		 pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		 pabort("can't get bits per word");
}

void set_spi_speed(int fd, uint32_t speed)
{
	int ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		 pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		 pabort("can't get max speed hz");	
}

void prepare(int fd)
{
	unsigned char ts[] = {0x04,0x00,0x40};
	SPI_SEL_CPLD();
	transfer(fd, ts, 3);
	SPI_SEL_DEV();
}

void finish(int fd)
{
	unsigned char ts[] = {0x04,0x00,0x00};
	SPI_SEL_CPLD();
	transfer(fd, ts, 3);
}


void config(int fd);
int main(int argc, char *argv[])
{
	int ret = 0;
	int fd;

	fd = open(SPI_NAME, O_RDWR);
	if (fd < 0)
	{
		pabort("Can't open device");
	}

	set_spi_bits(fd, SPI_BITS);
	set_spi_mode(fd, SPI_MODE);
	set_spi_speed(fd, 1000000);
	
	prepare(fd);

	config(fd);

	finish(fd);
	close(fd);
	return 0;
}

struct stPLL pll_cfg[] = {
{0x40,0xff},
{0x30,0x3fd},
{0x2f,0x8cf},
{0x2e,0xf20},
{0x2d,0x0},
{0x2c,0x0},
{0x2b,0x0},
{0x2a,0x0},
{0x29,0x3e8},
{0x28,0x0},
{0x27,0x8104},
{0x26,0x1f4},
{0x25,0x4000},
{0x24,0x0},
{0x23,0x1d},
{0x22,0xc3d0},
{0x21,0x4210},
{0x20,0x4210},
{0x1f,0x81},
{0x1e,0x34},
{0x1d,0x84},
{0x1c,0x2924},
{0x18,0x509},
{0x17,0x8842},
{0x13,0x965},
{0xe,0xffc},
{0xd,0x4000},
{0xc,0x7001},
{0xb,0x18},
{0xa,0x10d8},
{0x9,0x302},
{0x8,0x1084},
{0x7,0x28b2},
{0x1,0x808},
{0x0,0x223c}
};

void config(int fd)
{
	int regs = ARRAY_SIZE(pll_cfg);
	int i;
	unsigned char ts[] = {0x04,0x00,0x00};

	for(i=0; i<regs; i++)
	{
		unsigned short value;
		value = pll_cfg[i].val;

		ts[0] = pll_cfg[i].reg;
		ts[1] = ( value >> 8) & 0xff;
		ts[2] = value & 0xff;
		transfer(fd, ts, 3);
	}
}
