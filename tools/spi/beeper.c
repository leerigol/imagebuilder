#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "spi_bus.h"

 
static void pabort(const char *s)
{
      perror(s);
      abort();
}


static int transfer(int fd, unsigned char *ts_buf, unsigned int len)
{
	int rx = 0;
	int ret;
	struct spi_ioc_transfer tr = {
             .tx_buf = (unsigned long)ts_buf,   //定义发送缓冲区指针
             .rx_buf = (unsigned long)&rx,   //定义接收缓冲区指针
             .len = len,
             .delay_usecs = SPI_DELY};

    	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);//执行spidev.c中ioctl的default进行数据传输

	if (ret == 1)
	{
       pabort("can't send spi message");
	}

	rx  =  rx >> 8;
	ret =  rx & 0xff;
	rx  =  rx >> 8;
	rx  =  rx | (ret << 8);
	return rx;	
}


unsigned long simple_strtol(const char *cp, char **endp,unsigned int base)
{
	unsigned long result = 0;
	unsigned long value;

	if (*cp == '0') {
		cp++;
		if ((*cp == 'x') && isxdigit(cp[1])) {
			base = 16;
			cp++;
		}

		if (!base)
			base = 8;
	}

	if (!base)
		base = 10;

	while (isxdigit(*cp) && (value = isdigit(*cp) ? *cp-'0' : (islower(*cp)
	    ? toupper(*cp) : *cp)-'A'+10) < base) {
		result = result*base + value;
		cp++;
	}

	if (endp)
		*endp = (char *)cp;

	return result;
}



int main(int argc, char *argv[])
{
	unsigned char ts[] = {0x02,0x00,0x03};
	int fd = open(SPI_NAME, O_RDWR);       //打开"/dev/spidev1.0"
	if (fd < 0)
	{
	  pabort("can't open device");
	}

	SPI_SEL_CPLD();

	if(argc < 2 )
	{	
		 transfer(fd, ts, sizeof(ts));
	}
	else
	{
		if(strlen(argv[1]) == 2 && strncmp(argv[1], "-h", 2) == 0)
		{
			printf("0-Off\n");
			printf("1-Once\n");
			printf("2-Interval\n");
			printf("3-Continuous\n");
			return 0;
		}
		int input = simple_strtol(argv[1],NULL, 10) & 0xff;
		if(input == 0)
		{
			ts[2] = 0;
		}
		else if(input == 1)
		{
			ts[2] = 3;
		}
		else if(input == 2)
		{
			ts[2] = 2;
		}
		else if(input == 3)
		{
			ts[2] = 1;
		}		
	}
	transfer(fd, ts, sizeof(ts));
	usleep(10);

	close(fd);
	return 0;
}

