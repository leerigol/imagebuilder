#!/bin/bash

task_index=(0 1 2 3 4)
task_name=(spi2cpld spi2dev spi2k7 beeper spi2pll)

CC=arm-xilinx-linux-gnueabi-gcc

DT1=~/workspace/nfs/rigol/tools/
DT2=~/workspace/flamingo/images/rigol/tools/

for i in ${task_index[@]}
do	
	filename=${task_name[$i]}
	$CC $filename.c -o $filename
	cp  $filename $DT1
	cp  $filename $DT2
done

