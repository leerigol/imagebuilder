#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "spi_bus.h"

static void pabort(const char *s)
{	
	printf(s);
	exit(1);
}


static uint32_t BUFSZ = 4096;
static uint32_t BITSZ = 0x1000000;//16m

static int transfer(int fd, unsigned char *ts_buf, unsigned int len)
{
	int rx = 0;
	int ret;
	struct spi_ioc_transfer tr = {
             .tx_buf = (unsigned long)ts_buf,   //定义发送缓冲区指针
             .rx_buf = (unsigned long)&rx,   //定义接收缓冲区指针
             .len = len,
             .delay_usecs = SPI_DELY};

    ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);//执行spidev.c中ioctl的default进行数据传输

	if (ret == 1)
	{
        pabort("can't send spi message");
	}
	rx  =  rx >> 8;
	ret =  rx & 0xff;
	rx  =  rx >> 8;
	rx  =  rx | (ret << 8);
	return rx;
}


void set_spi_mode(int fd, uint8_t mode)
{
	int ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		 pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		 pabort("can't get spi mode");

}

void set_spi_bits(int fd, uint8_t bits)
{
	int ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		 pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		 pabort("can't get bits per word");
}

void set_spi_speed(int fd, uint32_t speed)
{
	int ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		 pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		 pabort("can't get max speed hz");	
}

int read_status(int fd)
{
	unsigned char ts[] = {0x0d,0x00, 0x00};
	int ret = transfer(fd, ts, 3);;
	printf("ret = 0x%x\n", ret);
	return ret;
}


#define RETRY_TIMES 16
int prepare_for_send(int fd)
{
	int i=0;
	int done = 0;
	//set Program_B low
	unsigned char ts[] = {0x01,0x00, 0x00};

	SPI_SEL_CPLD();
	{	
		unsigned char sel[] = {0x04,0x00 ,0x00};//slave
		transfer(fd, sel, 3);
	}
	for(i=0; i<RETRY_TIMES; i++)
	{
		transfer(fd, ts, 3);
		usleep(10000);//delay 1ms
		
		done = read_status(fd) & 0x200;
		if(done == 0)
		{
			break;
		}
	}
	if(i==RETRY_TIMES) return -1;
	//set Program_B high
	usleep(1);//delay 1us
	ts[2] = 0x80;
	for(i=0; i<RETRY_TIMES; i++)
	{
		transfer(fd, ts, 3);
		usleep(10000);//delay 1ms
		
		done = read_status(fd) & 0x200;
		if(done == 0x200)
		{
			break;
		}
	}
	if(i==RETRY_TIMES) return -1;

	return 0;	
}

int wait_for_done(int fd)
{
	int i = 0;
	int done = 0;

	SPI_SEL_CPLD();
	//deselect device K7;	
	{	
		unsigned char ts[] = {0x04,0x00,0x00};// master and slave
		transfer(fd, ts, 3);
	}

	for(i=0; i<RETRY_TIMES; i++)
	{
		usleep(1000);//delay 1ms
		
		done = read_status(fd) & 0x400;
		if(done == 0x400)
		{
			break;
		}
		//shilf_clk(fd);//push
	}
	if(i==RETRY_TIMES) return -1;
	return 0;
}

//sending k7 bit stream
int down_K7(int fd, const char* bit_name)
{
	int   nFileSize = 0;
	unsigned char* pDataBuf = NULL;

	FILE * pFile = fopen(bit_name, "r");
	if(pFile)
	{
		pDataBuf = (unsigned char*)malloc(BITSZ);
		if(pDataBuf)
		{
			int step;
			int i;
			int nLeftSize;
			unsigned char* pBits = NULL;
			
			SPI_SEL_DEV();

			nFileSize = fread(pDataBuf, 1, BITSZ, pFile);

			//printf("Sending :0x%x\n", nFileSize);
			pBits = pDataBuf;
			step = nFileSize / BUFSZ;
			while(step)
			{
				write(fd, pBits, BUFSZ);
				pBits += BUFSZ;
				step--;
			}
			nLeftSize = pDataBuf + nFileSize - pBits;			
			write(fd, pBits, nLeftSize);

			usleep(10000);//10ms

			free(pDataBuf);
			//wait for done
			SPI_SEL_CPLD();
		}
		fclose(pFile);
		return 0;			
	}

	return -1;
}
int main(int argc, char *argv[])
{
	int ret = 0;
	int fd;

	char* pK160M = "/rigol/K160M_TOP.bit";
	char* pK160S = NULL;


	if(argc > 1 )
	{
		pK160M = argv[1];
	}

	if( argc > 2 )
	{
		pK160S = argv[2];
	}
	
	if(argc == 0 )
	{
		printf("spi2k7 K7_Master [K7_Slave]----Build:%s\n", __DATE__);
		printf("Down default %s\n", pK160M);
	}

	fd = open(SPI_NAME, O_RDWR);
	if (fd < 0)
	{
		pabort("Can't open device");
	}

	set_spi_bits(fd, SPI_BITS);
	set_spi_mode(fd, SPI_MODE);
	set_spi_speed(fd, 20000000);

	printf("Setting program_B...\n");
	
	if(prepare_for_send(fd) != 0)
	{
		close(fd);
		pabort("!!!Can not set for program_b \n");
	}

	
	//master
	printf("Down K7 master\n");
	//select device K7 master;	
	{	
		unsigned char sel[] = {0x04,0x02 ,0x00};//slave
		transfer(fd, sel, 3);
	}
	if(down_K7(fd, pK160M) != 0)
	{
		close(fd);
		pabort("!!!Down K7 master failed\n");
	}

	if( pK160S )
	{
		printf("Down K7 slave\n");
		if(down_K7(fd, pK160S) != 0)
		{
			close(fd);
			pabort("!!!Down K7 slave failed\n");
		}
	}

//////////////////////////////////////////////////////////////////
	printf("Waiting for DONE...\n");
	if(wait_for_done(fd) != 0)
	{
		close(fd);
		pabort("!!!Can not wait for DONE \n");
	}

	
	close(fd);
	return 0;
}

