#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#define I2C_FILE_NAME "/dev/i2c-0"
#define SIZE 16

static int get_i2c_register(int file,  
                            unsigned short addr,  
                            unsigned short reg,  
                            unsigned char *val,int len);
static int set_i2c_register(int file,  
                            unsigned short addr,  
                            unsigned short reg,  
                            unsigned char value);

void usage(char* app)
{
	printf("%s -r reg [len]\n",  app);
	printf("%s -w reg val\n", app);
}

unsigned long simple_strtol(const char *cp, char **endp,unsigned int base)
{
	unsigned long result = 0;
	unsigned long value;

	if (*cp == '0') {
		cp++;
		if ((*cp == 'x') && isxdigit(cp[1])) {
			base = 16;
			cp++;
		}

		if (!base)
			base = 8;
	}

	if (!base)
		base = 10;

	while (isxdigit(*cp) && (value = isdigit(*cp) ? *cp-'0' : (islower(*cp)
	    ? toupper(*cp) : *cp)-'A'+10) < base) {
		result = result*base + value;
		cp++;
	}

	if (endp)
		*endp = (char *)cp;

	return result;
}

int main(int argc, char *argv[])
{
	int addr = 0x52; // i2c address
	int i2c_file;
	int i=0;
	int rw = 0;//0 w, 1 r
	int reg;
	int val;
	if(argc < 2)
	{
		usage( argv[0] );
		return 0;
	}

	if( strncmp( argv[1] , "-r", 2 ) == 0 )
	{
		rw = 1;
	}
	else if( strncmp( argv[1] , "-w", 2 ) == 0 )
	{
		rw = 0;
	}
	else
	{
		usage( argv[0] );
		return 0;
	}
	
	if( (rw == 0 && argc < 4) || (rw == 1 && argc < 3) )
	{
		usage( argv[0] );
		return 0;
	}

	reg = simple_strtol(argv[2], NULL, 16);
	if(rw == 0)
	{
		val = simple_strtol(argv[3], NULL, 16);
	}
	

	if ((i2c_file = open(I2C_FILE_NAME, O_RDWR)) < 0) 
	{  
        perror("Unable to open i2c control file");  
        exit(1);  
    }
	if( rw == 0 )
	{
		//for(i=0; i<(8192/256); i++)
		{
			//reg = i*256;
			set_i2c_register(i2c_file, addr, reg, val);
		}
	}
	else
	{
#if 1
		unsigned char buf[SIZE];
		val=0;
		int i;
		//for(i=0; i<(4096/256); i++)
		{
			int len = 16;
			if( argc == 4 )	
			{	
				len = simple_strtol(argv[3], NULL, 16);
			}
			get_i2c_register(i2c_file, addr, reg, (unsigned char*)buf, len);

			for(i=0;i<len;i++)
			{
				printf("%02x,",buf[i]);
				if( (i+1) % 16 == 0)
				{
					printf("\n");
				}
			}
			//printf("Read 0x%x\n", buf[0]);
			usleep(300);
		}
#else
		char buf[SIZE];
		int ret;
		ioctl(i2c_file, I2C_SLAVE,addr);
		
		ret = read(i2c_file, buf, sizeof(buf));
		printf("read count = %d\n", ret);
#endif
	}
	
	close(i2c_file);

	return 0;
}


static int set_i2c_register(int file,  
                            unsigned short addr,  
                            unsigned short reg,  
                            unsigned char value) {  
  
    unsigned char outbuf[SIZE+2];
	int i=0;

    struct i2c_rdwr_ioctl_data packets;  
    struct i2c_msg messages[1];  
  
    messages[0].addr  = addr;  
    messages[0].flags = 0;  
    messages[0].len   = sizeof(outbuf);  
    messages[0].buf   = outbuf;  
  
    
    outbuf[0] = (reg >> 8) & 0xff;
    outbuf[1] = reg & 0xff;
///	outbuf[2] = value;
	
	for(i=0; i<SIZE; i++)
	{
		outbuf[2+i] = 0xff;
	}

    /* Transfer the i2c packets to the kernel and verify it worked */  
    packets.msgs  = messages;  
    packets.nmsgs = 1;  
    if(ioctl(file, I2C_RDWR, &packets) < 0) {  
        perror("Unable to send data");  
        return 1;  
    }  
  
    return 0;  
}  
  
  
static int get_i2c_register(int file,  
                            unsigned short addr,  
                            unsigned short reg,  
                            unsigned char *val,int len) {  

    unsigned char outbuf[2];
    struct i2c_rdwr_ioctl_data packets;  
    struct i2c_msg messages[2];  
  
    /* 
     * In order to read a register, we first do a "dummy write" by writing 
     * 0 bytes to the register we want to read from.  This is similar to 
     * the packet in set_i2c_register, except it's 1 byte rather than 2. 
     */  
    outbuf[0] = (reg >> 8) & 0xff;
    outbuf[1] = reg & 0xff; 
    messages[0].addr  = addr;
    messages[0].flags = 0;
    messages[0].len   = sizeof(outbuf);  
    messages[0].buf   = outbuf;
  
    /* The data will get returned in this structure */  
    messages[1].addr  = addr;  
    messages[1].flags = I2C_M_RD/* | I2C_M_NOSTART*/;  
    messages[1].len   = len;  
    messages[1].buf   = val;
  
    /* Send the request to the kernel and get the result back */  
    packets.msgs      = messages;  
    packets.nmsgs     = 2;
    if(ioctl(file, I2C_RDWR, &packets) < 0) {
        perror("Unable to send data");  
        return 1;  
    } 
    return 0;  
}  
  
