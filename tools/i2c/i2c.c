#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#define I2C_FILE_NAME "/dev/i2c-0"

static unsigned char gtp[] = {
0x61,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x01,0xC0,0x00,0xB6,0xB4,0x92,
0x82,0x0C,0x81,0x80,0x00,0x03,0x8C,0x06,0x40,0x00,0x00,0x00,0x9F,0xFF,0xE0,0x80,
0x00,0x81,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x00,0x01,0x00,0xA0,0x00,
0x00,0x0C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x00,0x01,0x00,0xA0,0x00,
0x00,0x0C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x00,0x01,0x00,0xA0,0x00,
0x00,0x0C,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00,0xA0,0x00,
0x63,0x01,0x63,0x01,0x63,0x01,0x63,0x01,0xFE,0x7C};

static int get_i2c_register(int file,  
                            unsigned char addr,  
                            unsigned char reg,  
                            unsigned char *val);
static int set_i2c_register(int file,  
                            unsigned char addr,  
                            unsigned char reg,  
                            unsigned char value);
int main()
{
	int addr = 0x6a; // i2c address
	int i2c_file;
	int i=0;
	
	if ((i2c_file = open(I2C_FILE_NAME, O_RDWR)) < 0) 
	{  
        perror("Unable to open i2c control file");  
        exit(1);  
    }
	//printf("i2c_file=%d\n", i2c_file);
	for(i=0; i<sizeof(gtp); i++)
	{
		if( 1 == set_i2c_register(i2c_file, addr, (unsigned char)i, gtp[i] ) )
		{
			printf("set error\n");
			break;
		}
	}
	close(i2c_file);

	return 0;
}
static int set_i2c_register(int file,  
                            unsigned char addr,  
                            unsigned char reg,  
                            unsigned char value) {  
  
    unsigned char outbuf[2];  
    struct i2c_rdwr_ioctl_data packets;  
    struct i2c_msg messages[1];  
  
    messages[0].addr  = addr;  
    messages[0].flags = 0;  
    messages[0].len   = sizeof(outbuf);  
    messages[0].buf   = outbuf;  
  
    /* The first byte indicates which register we'll write */  
    outbuf[0] = reg;  
  
    /*  
     * The second byte indicates the value to write.  Note that for many 
     * devices, we can write multiple, sequential registers at once by 
     * simply making outbuf bigger. 
     */  
    outbuf[1] = value;  
  
    /* Transfer the i2c packets to the kernel and verify it worked */  
    packets.msgs  = messages;  
    packets.nmsgs = 1;  
    if(ioctl(file, I2C_RDWR, &packets) < 0) {  
        perror("Unable to send data");  
        return 1;  
    }  
  
    return 0;  
}  
  
  
static int get_i2c_register(int file,  
                            unsigned char addr,  
                            unsigned char reg,  
                            unsigned char *val) {  
    unsigned char inbuf, outbuf;  
    struct i2c_rdwr_ioctl_data packets;  
    struct i2c_msg messages[2];  
  
    /* 
     * In order to read a register, we first do a "dummy write" by writing 
     * 0 bytes to the register we want to read from.  This is similar to 
     * the packet in set_i2c_register, except it's 1 byte rather than 2. 
     */  
    outbuf = reg;  
    messages[0].addr  = addr;  
    messages[0].flags = 0;  
    messages[0].len   = sizeof(outbuf);  
    messages[0].buf   = &outbuf;  
  
    /* The data will get returned in this structure */  
    messages[1].addr  = addr;  
    messages[1].flags = I2C_M_RD/* | I2C_M_NOSTART*/;  
    messages[1].len   = sizeof(inbuf);  
    messages[1].buf   = &inbuf;  
  
    /* Send the request to the kernel and get the result back */  
    packets.msgs      = messages;  
    packets.nmsgs     = 2;  
    if(ioctl(file, I2C_RDWR, &packets) < 0) {  
        perror("Unable to send data");  
        return 1;  
    }  
    *val = inbuf;  
  
    return 0;  
}  
  
