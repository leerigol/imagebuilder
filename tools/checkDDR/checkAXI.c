#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>


unsigned long simple_strtol(const char *cp, char **endp,unsigned int base)
{
	unsigned long result = 0;
	unsigned long value;

	if (*cp == '0') {
		cp++;
		if ((*cp == 'x') && isxdigit(cp[1])) {
			base = 16;
			cp++;
		}

		if (!base)
			base = 8;
	}

	if (!base)
		base = 10;

	while (isxdigit(*cp) && (value = isdigit(*cp) ? *cp-'0' : (islower(*cp)
	    ? toupper(*cp) : *cp)-'A'+10) < base) {
		result = result*base + value;
		cp++;
	}

	if (endp)
		*endp = (char *)cp;

	return result;
}
#define BUS_SIZE (64*1024)
int main(int argc, char*argv[])
{
	unsigned int offset = 0x1c9c;
	unsigned int value  = 0xffff;
	unsigned int axi_bus_base = 0;
	int rdonly = 1;
	int fd = open("/dev/mem", O_RDWR);
	if(fd == -1)
	{
	  printf("Cant' open /dev/mem\n");
	  exit(1);
	}

	axi_bus_base = (unsigned int)mmap(NULL,BUS_SIZE,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0x83c00000);

	if(argc > 1)
	{
		offset =  simple_strtol(argv[1], NULL , 10);
	}
	
	if(argc > 2)
	{
		value =  simple_strtol(argv[2], NULL , 10);
		rdonly = 0;
	}

	if(rdonly)
	{
		offset = *(volatile unsigned int*)(axi_bus_base + offset);
		printf("read result:0x%x\n", offset);
		return offset & 0xff;
	}
	else
	{
		*(volatile unsigned int*)(axi_bus_base + offset) = value;		
	}
	
	//printf("spu.ver=0x%x\n", *(volatile unsigned int*)(axi_bus_base + 0x1400));
	//printf("wpu.ver=0x%x\n", *(volatile unsigned int*)(axi_bus_base + 0x400));
	//printf("scu.ver=0x%x\n", *(volatile unsigned int*)(axi_bus_base + 0x1c00));
	
	munmap((void*)axi_bus_base, BUS_SIZE); //destroy map memory
	close(fd);
	return 0;
}

