#-------------------------------------------------
#
# Project created by QtCreator 2016-07-30T14:39:05
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = cfger
CONFIG   += console
CONFIG   -= app_bundle

CONFIG   += static

TEMPLATE = app

platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += ForPC
}

LIBS += -L./lib$$(PLATFORM)

LIBS += -laes

SOURCES += main.cpp
