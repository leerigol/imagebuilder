#include <QCoreApplication>
#include <QFile>
#include <QDataStream>
#include <QDebug>
#include "aes.h"

#define  uint32_t       unsigned long
#define  Bytef          unsigned char
#define  uInt           unsigned int
#define  uint8_t        unsigned char
#define  tole(x)        (x)
#define  cpu_to_le32(x) (x)
#define  le32_to_cpu(x) (x)
#define  DO_CRC(x)      (crc = tab[(crc ^ (x)) & 255] ^ (crc >> 8))

#ifdef ForPC
#define ENV_FILE    "/home/rigolee/workspace/flamingo/images/tools/build/env.txt"
#else
#define ENV_FILE    "/tmp/env.bin"
#define SysInfo     "/tmp/sysinfo.txt"
#endif

int trim_file(QString filename);
int aes_file(QString input, QString output, int aes_type);
uint32_t  crc32 (uint32_t crc, const Bytef *p, uInt len);
bool build_map_table(QByteArray& ba);
void env_help(void);
void env_print();
QString env_read(QString& name);
int env_compare(QString& name, QString& value);
int env_set(QString& name, QString& value);
QList<QString> env_list_data;

QList<QString> info_list;

int main(int argc, char *argv[])
{
    int ret = 1;

    //info_list.append("model");
    info_list.append("vendor");
    info_list.append("softver");
    info_list.append("bootver");
    info_list.append("builddate");

    //info_list.append("ethaddr");

    if(argc == 3 && strncmp(argv[1], "-t", 2) == 0)
    {
        QString name(argv[2]);
        ret = trim_file(name);
        return ret;
    }
    else if(argc == 4 )
    {
        QString input(argv[2]);
        QString output(argv[3]);

        //qDebug() << input << output;

        if(strncmp(argv[1], "-d", 2) == 0)
        {
           return aes_file(input,output, AES_DECRYPT);
        }
        else if(strncmp(argv[1], "-e", 2) == 0)
        {
           return aes_file(input,output, AES_ENCRYPT);
        }
    }

    QFile env_file(QString(ENV_FILE));
    if(QFile::exists(QString(ENV_FILE)) && env_file.open(QIODevice::ReadWrite))
    {
        QByteArray env_data = env_file.readAll();

        Bytef *p = (Bytef*)env_data.data();
        uint32_t crc = *(uint32_t*)p;
        p += 4;

        uint32_t old_crc = crc32(0, p, env_data.size() - 4);

        if(crc != old_crc )
        {
            qDebug() << "crc error";
            ret = 1;
        }
        else
        {
            bool build_ok = build_map_table(env_data);
            if( build_ok)
            {
                if(argc == 1)
                {
                    env_print();
                }
                else if(argc == 2)
                {
                    if(strncmp(argv[1], "-h", 2) == 0)
                    {
                        env_help();
                    }
                }
                else if(argc == 3)
                {
                    QString name(argv[2]);
                    // reading env
                    //-r bootver
                    //-r softver
                    if(strncmp(argv[1], "-r", 2) == 0)
                    {
                        qDebug() << env_read(name);
                        ret = 0;
                    }
                    //read all information to /sys.txt
                    else if(strncmp(argv[1], "-i", 2) == 0)
                    {
                        QFile file(argv[2]);
                        if(file.open(QFile::WriteOnly | QFile::Truncate))
                        {
                            QTextStream _out( &file );
                            QString env_name;
                            for(int i=0; i<info_list.size(); i++)
                            {
                                env_name = info_list.at(i);
                                if(env_name.compare("builddate") == 0)
                                {
                                    _out << env_name << "='" << env_read(env_name) << "'\n";
                                }
                                else
                                {
                                    _out << env_name << "=" << env_read(env_name) << "\n";
                                }
                            }
                            file.close();
                        }
                        else
                        {
                            qDebug() << "Reading information FAILED.";
                        }
                    }
                }
                else if(argc == 4)
                {
                    QString name(argv[2]);
                    QString value(argv[3]);

                    //-c softver val
                    //-c bootver val
                    if(strncmp(argv[1], "-c", 2) == 0)
                    {
                        ret = env_compare(name, value);
                    }
                    //-s softver val
                    //-s bootver val
                    else if(strncmp(argv[1], "-s", 2) == 0)
                    {
                        ret = env_set(name,value);
                        //saving
                        memset(env_data.data(), 0, 0x20000);
                        for(int i=0; i<env_list_data.count(); i++)
                        {
                            memcpy(p, env_list_data.at(i).toLatin1().data(), env_list_data.at(i).toLatin1().size());
                            p += env_list_data.at(i).toLatin1().size();
                            *p++ = 0;
                        }

                        p = (Bytef*)env_data.data();
                        crc = crc32(0, p+4,  0x40000 - 4);
                        *(uint32_t*)p = crc;
                        env_file.seek(0);
                        env_file.write(env_data);

                        //qDebug() << name + "=" + value;
                    }
                }
                env_list_data.clear();
            }
        }
        env_file.close();
    }
    else
    {
        qDebug() << ENV_FILE << "not exist";
    }
    return ret ;
}



const uint32_t crc_table[256] = {
tole(0x00000000L), tole(0x77073096L), tole(0xee0e612cL), tole(0x990951baL),
tole(0x076dc419L), tole(0x706af48fL), tole(0xe963a535L), tole(0x9e6495a3L),
tole(0x0edb8832L), tole(0x79dcb8a4L), tole(0xe0d5e91eL), tole(0x97d2d988L),
tole(0x09b64c2bL), tole(0x7eb17cbdL), tole(0xe7b82d07L), tole(0x90bf1d91L),
tole(0x1db71064L), tole(0x6ab020f2L), tole(0xf3b97148L), tole(0x84be41deL),
tole(0x1adad47dL), tole(0x6ddde4ebL), tole(0xf4d4b551L), tole(0x83d385c7L),
tole(0x136c9856L), tole(0x646ba8c0L), tole(0xfd62f97aL), tole(0x8a65c9ecL),
tole(0x14015c4fL), tole(0x63066cd9L), tole(0xfa0f3d63L), tole(0x8d080df5L),
tole(0x3b6e20c8L), tole(0x4c69105eL), tole(0xd56041e4L), tole(0xa2677172L),
tole(0x3c03e4d1L), tole(0x4b04d447L), tole(0xd20d85fdL), tole(0xa50ab56bL),
tole(0x35b5a8faL), tole(0x42b2986cL), tole(0xdbbbc9d6L), tole(0xacbcf940L),
tole(0x32d86ce3L), tole(0x45df5c75L), tole(0xdcd60dcfL), tole(0xabd13d59L),
tole(0x26d930acL), tole(0x51de003aL), tole(0xc8d75180L), tole(0xbfd06116L),
tole(0x21b4f4b5L), tole(0x56b3c423L), tole(0xcfba9599L), tole(0xb8bda50fL),
tole(0x2802b89eL), tole(0x5f058808L), tole(0xc60cd9b2L), tole(0xb10be924L),
tole(0x2f6f7c87L), tole(0x58684c11L), tole(0xc1611dabL), tole(0xb6662d3dL),
tole(0x76dc4190L), tole(0x01db7106L), tole(0x98d220bcL), tole(0xefd5102aL),
tole(0x71b18589L), tole(0x06b6b51fL), tole(0x9fbfe4a5L), tole(0xe8b8d433L),
tole(0x7807c9a2L), tole(0x0f00f934L), tole(0x9609a88eL), tole(0xe10e9818L),
tole(0x7f6a0dbbL), tole(0x086d3d2dL), tole(0x91646c97L), tole(0xe6635c01L),
tole(0x6b6b51f4L), tole(0x1c6c6162L), tole(0x856530d8L), tole(0xf262004eL),
tole(0x6c0695edL), tole(0x1b01a57bL), tole(0x8208f4c1L), tole(0xf50fc457L),
tole(0x65b0d9c6L), tole(0x12b7e950L), tole(0x8bbeb8eaL), tole(0xfcb9887cL),
tole(0x62dd1ddfL), tole(0x15da2d49L), tole(0x8cd37cf3L), tole(0xfbd44c65L),
tole(0x4db26158L), tole(0x3ab551ceL), tole(0xa3bc0074L), tole(0xd4bb30e2L),
tole(0x4adfa541L), tole(0x3dd895d7L), tole(0xa4d1c46dL), tole(0xd3d6f4fbL),
tole(0x4369e96aL), tole(0x346ed9fcL), tole(0xad678846L), tole(0xda60b8d0L),
tole(0x44042d73L), tole(0x33031de5L), tole(0xaa0a4c5fL), tole(0xdd0d7cc9L),
tole(0x5005713cL), tole(0x270241aaL), tole(0xbe0b1010L), tole(0xc90c2086L),
tole(0x5768b525L), tole(0x206f85b3L), tole(0xb966d409L), tole(0xce61e49fL),
tole(0x5edef90eL), tole(0x29d9c998L), tole(0xb0d09822L), tole(0xc7d7a8b4L),
tole(0x59b33d17L), tole(0x2eb40d81L), tole(0xb7bd5c3bL), tole(0xc0ba6cadL),
tole(0xedb88320L), tole(0x9abfb3b6L), tole(0x03b6e20cL), tole(0x74b1d29aL),
tole(0xead54739L), tole(0x9dd277afL), tole(0x04db2615L), tole(0x73dc1683L),
tole(0xe3630b12L), tole(0x94643b84L), tole(0x0d6d6a3eL), tole(0x7a6a5aa8L),
tole(0xe40ecf0bL), tole(0x9309ff9dL), tole(0x0a00ae27L), tole(0x7d079eb1L),
tole(0xf00f9344L), tole(0x8708a3d2L), tole(0x1e01f268L), tole(0x6906c2feL),
tole(0xf762575dL), tole(0x806567cbL), tole(0x196c3671L), tole(0x6e6b06e7L),
tole(0xfed41b76L), tole(0x89d32be0L), tole(0x10da7a5aL), tole(0x67dd4accL),
tole(0xf9b9df6fL), tole(0x8ebeeff9L), tole(0x17b7be43L), tole(0x60b08ed5L),
tole(0xd6d6a3e8L), tole(0xa1d1937eL), tole(0x38d8c2c4L), tole(0x4fdff252L),
tole(0xd1bb67f1L), tole(0xa6bc5767L), tole(0x3fb506ddL), tole(0x48b2364bL),
tole(0xd80d2bdaL), tole(0xaf0a1b4cL), tole(0x36034af6L), tole(0x41047a60L),
tole(0xdf60efc3L), tole(0xa867df55L), tole(0x316e8eefL), tole(0x4669be79L),
tole(0xcb61b38cL), tole(0xbc66831aL), tole(0x256fd2a0L), tole(0x5268e236L),
tole(0xcc0c7795L), tole(0xbb0b4703L), tole(0x220216b9L), tole(0x5505262fL),
tole(0xc5ba3bbeL), tole(0xb2bd0b28L), tole(0x2bb45a92L), tole(0x5cb36a04L),
tole(0xc2d7ffa7L), tole(0xb5d0cf31L), tole(0x2cd99e8bL), tole(0x5bdeae1dL),
tole(0x9b64c2b0L), tole(0xec63f226L), tole(0x756aa39cL), tole(0x026d930aL),
tole(0x9c0906a9L), tole(0xeb0e363fL), tole(0x72076785L), tole(0x05005713L),
tole(0x95bf4a82L), tole(0xe2b87a14L), tole(0x7bb12baeL), tole(0x0cb61b38L),
tole(0x92d28e9bL), tole(0xe5d5be0dL), tole(0x7cdcefb7L), tole(0x0bdbdf21L),
tole(0x86d3d2d4L), tole(0xf1d4e242L), tole(0x68ddb3f8L), tole(0x1fda836eL),
tole(0x81be16cdL), tole(0xf6b9265bL), tole(0x6fb077e1L), tole(0x18b74777L),
tole(0x88085ae6L), tole(0xff0f6a70L), tole(0x66063bcaL), tole(0x11010b5cL),
tole(0x8f659effL), tole(0xf862ae69L), tole(0x616bffd3L), tole(0x166ccf45L),
tole(0xa00ae278L), tole(0xd70dd2eeL), tole(0x4e048354L), tole(0x3903b3c2L),
tole(0xa7672661L), tole(0xd06016f7L), tole(0x4969474dL), tole(0x3e6e77dbL),
tole(0xaed16a4aL), tole(0xd9d65adcL), tole(0x40df0b66L), tole(0x37d83bf0L),
tole(0xa9bcae53L), tole(0xdebb9ec5L), tole(0x47b2cf7fL), tole(0x30b5ffe9L),
tole(0xbdbdf21cL), tole(0xcabac28aL), tole(0x53b39330L), tole(0x24b4a3a6L),
tole(0xbad03605L), tole(0xcdd70693L), tole(0x54de5729L), tole(0x23d967bfL),
tole(0xb3667a2eL), tole(0xc4614ab8L), tole(0x5d681b02L), tole(0x2a6f2b94L),
tole(0xb40bbe37L), tole(0xc30c8ea1L), tole(0x5a05df1bL), tole(0x2d02ef8dL)
};

uint32_t  crc32_no_comp(uint32_t crc, const Bytef *buf, uInt len)
{
    const uint32_t *tab = crc_table;
    const uint32_t *b =(const uint32_t *)buf;
    size_t rem_len;
#ifdef DYNAMIC_CRC_TABLE
    if (crc_table_empty)
      make_crc_table();
#endif
    crc = cpu_to_le32(crc);
    /* Align it */
    if (((long)b) & 3 && len) {
     uint8_t *p = (uint8_t *)b;
     do {
          DO_CRC(*p++);
     } while ((--len) && ((long)p)&3);
     b = (uint32_t *)p;
    }

    rem_len = len & 3;
    len = len >> 2;
    for (--b; len; --len) {
     /* load data 32 bits wide, xor data 32 bits wide. */
     crc ^= *++b; /* use pre increment for speed */
     DO_CRC(0);
     DO_CRC(0);
     DO_CRC(0);
     DO_CRC(0);
    }
    len = rem_len;
    /* And the last few bytes */
    if (len) {
     uint8_t *p = (uint8_t *)(b + 1) - 1;
     do {
          DO_CRC(*++p); /* use pre increment for speed */
     } while (--len);
    }

    return le32_to_cpu(crc);
}
#undef DO_CRC

uint32_t  crc32 (uint32_t crc, const Bytef *p, uInt len)
{
     return crc32_no_comp(crc ^ 0xffffffffL, p, len) ^ 0xffffffffL;
}



bool build_map_table(QByteArray& ba)
{
    Q_ASSERT(ba.size() > 0);
    char *pData = ba.data();
    pData += 4; //jump crc
    char Buffer[1024];

    bool bReturn = true;
    while(*pData != 0)
    {
        memset(Buffer, 0, sizeof(Buffer));
        char *pBuf = Buffer;
        while(*pData)
        {
            *pBuf++ = *pData++;
        }

        QString name(Buffer);

        env_list_data.append(name);

        pData++;
    }

    return bReturn;
}


void env_print()
{
    for(int i=0; i<env_list_data.count(); i++)
    {
       qDebug() << env_list_data.at(i);
    }
}

QString env_read(QString& name)
{
    for(int i=0; i<env_list_data.count(); i++)
    {
        if( env_list_data.at(i).startsWith(name) )
        {
            int len = env_list_data.at(i).length();
            return env_list_data.at(i).right(len - name.length() - 1);
        }
    }
    return QString("NULL");
}

int env_compare(QString& name, QString& value)
{
    int len = value.length();


    if(name.startsWith("softver"))
    {
        //jump the temp version
        if(len != 14)
        {
            return 1;
        }
        len -= 3;
    }

    for(int i=0; i<env_list_data.count(); i++)
    {
        if( env_list_data.at(i).startsWith(name) )
        {
            int ret = env_list_data.at(i).right(len).compare(value.right(len));
            //qDebug() << env_list_data.at(i).right(len) << value.right(len) ;
            if(ret < 0)
            {
                ret = 2;
            }
            else if( ret > 0)
            {
                ret = 1;
            }

            return ret;
        }
    }
    //qDebug() << "more than";
    return 1;
}

/**
 * @brief env_set
 * @param name
 * @param [value] if value is empty, then remove the item by name
 * @return
 */
int env_set(QString& name, QString& value)
{
    bool is_found = false;

    //qDebug() << env_list_data.size();
    for(int i=0; i<env_list_data.count(); i++)
    {
        //qDebug() << env_list_data.at(i);
        if(env_list_data.at(i).startsWith(name))
        {            
            env_list_data.removeAt(i);
            if(value.length() > 0)
            {
                env_list_data.append( name + "=" + value);
            }
            is_found = true;
            break;
        }
    }
    if(!is_found && value.length() > 0)
    {
        env_list_data.append(name + "=" + value);
    }

    return 0;
}

int trim_file(QString filename)
{
    int ret = 1;
    QFile file(filename);
    if(file.open(QIODevice::ReadWrite))
    {

        QByteArray ba;
        QByteArray wb;

        ba = file.readAll();
        file.close();

        char *p = ba.data();
        while(*p != '\0')
        {
            wb.append(*p++);
        }

        if(file.open(QIODevice::Truncate | QIODevice::WriteOnly) )
        {
            file.write(wb);
            file.close();
            ret = 0;
        }
    }
    return ret;
}


int aes_file(QString input, QString output, int aes_type)
{    
    unsigned char tmp_key[16];
    aes_context aes;
    int keysize = 128;
    int file_len = 0;

    extern unsigned char AES_KEY[16];

    memcpy(tmp_key, AES_KEY, sizeof(tmp_key));

    if(aes_type == AES_DECRYPT)
    {
        aes_setkey_dec(&aes, tmp_key, keysize);
    }
    else if(aes_type == AES_ENCRYPT)
    {
        aes_setkey_enc(&aes, tmp_key, keysize);
    }
    else
    {
        return 1;
    }

    QFile fileSrc(input);

    if(fileSrc.open(QIODevice::ReadOnly))
    {
        QByteArray baInput = fileSrc.readAll();
        fileSrc.close();

        file_len = baInput.size();

        if(file_len == 0)
        {
            qDebug() << "can not read src file";
        }

        if(file_len % 16)
        {
            //qDebug() << "Old" << file_len;
            file_len =  (file_len + 15) & 0xfffffff0;
            int padding = file_len - baInput.size();
            for(int i=0; i<padding; i++)
            {
                baInput.append((char)0xa);
            }
            // qDebug() << "New"<<file_len;
        }

        unsigned char* BUF = (unsigned char*)malloc(file_len);

        if(0 != aes_crypt_cbc(&aes,
                           aes_type,
                           file_len,
                           tmp_key,
                           (unsigned char*)baInput.data(), BUF) )
        {
           qDebug() << "AES ERROR at:" << __LINE__;
           free(BUF);
           return 1;
        }

        QFile fileDst(output);
        if(fileDst.open(QIODevice::WriteOnly | QIODevice::Truncate) )
        {
            int ret = 0;
            if ( file_len != fileDst.write( (const char*)BUF, file_len) )
            {
                qDebug() << "Write error";

                ret = 1;
            }
            free(BUF);
            fileDst.close();
            return ret;
        }
    }

    return 0;
}


void env_help(void)
{
    qDebug() << " -r name:read the value of name" << endl;
    qDebug() << " -i file:read model,version,date to file" << endl;
    qDebug() << " -c name value: compare bwtween the value of name with value" << endl;
    qDebug() << " -s name value: set the value of name" << endl;
    qDebug() << " -t file: remove the all zero of the file" << endl;
    qDebug() << " -d input output: decrypt the input to output by aes" << endl;
    qDebug() << " -e input output: crypt the input to output by aes" << endl;
    qDebug() << " -h : show this help information" << endl;
}
