#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>

#define ENV_FILE    "/tmp/env.bin"
#define CMD_SIZE 	(64*1024)
char cmd_buf[CMD_SIZE];
void main(int argc, char*argv[])
{
	int fd;
	char *cmd_ptr = NULL;
	int cmd_size;
	int ret = 1;

	if(argc < 4)
	{
		printf("checkapp -c bootpart x\n");
		exit(1);
	}
	if(strncmp(argv[1], "-c", 2) != 0)
	{
		exit(1);
	}
	
	fd = open(ENV_FILE, O_RDONLY);
	if (fd < 0) 
	{  
        printf("Unable to open env file:%s", ENV_FILE);  
        exit(1);
    }
	
	cmd_size = read(fd, cmd_buf, CMD_SIZE);
	close(fd);
	if(cmd_size > 0)
	{
		char *p = &cmd_buf[0];
		int  l = strlen(argv[2]);

		p+=4;
		while( p )
		{
			//printf("p=%s\n", p);
			if(strncmp(p, argv[2], l)  == 0)
			{
				break;
			}
			p += strlen(p) + 1 ;
		}
		if(p)
		{
			//printf("p=%s\n", p);
			p++;
			p+= strlen(argv[2]);
			
			ret = strncmp(p, argv[3], strlen(argv[3])) ;
			if(ret < 0 )
			{
				ret = 2;
			}
			else if(ret > 0)
			{
				ret = 1;
			}
		}
	}

	exit(ret);
}

