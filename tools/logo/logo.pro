#-------------------------------------------------
#
# Project created by QtCreator 2016-12-16T15:20:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = showLogo
TEMPLATE = app


SOURCES += main.cpp\
        csplashscreen.cpp

HEADERS  += csplashscreen.h

FORMS    += csplashscreen.ui
