#include <QFile>
#include "csplashscreen.h"
#include "ui_csplashscreen.h"


CSplashScreen::CSplashScreen(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CSplashScreen)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);

    connect(&m_DateTime,SIGNAL(timeout()),this,SLOT(updateTime()));
    m_DateTime.start(500);
    this->setObjectName(QStringLiteral("LOGO_LAYER"));
}

CSplashScreen::~CSplashScreen()
{
    delete ui;
}
void CSplashScreen::updateTime()
{
    if ( QFile::exists(FLAMINGO_FILE))
    {
        m_DateTime.stop();
        m_Movie.stop();
        exit(0);
    }
}

void CSplashScreen::setLogo(QString logoName)
{
    m_Movie.setFileName(logoName);
    ui->logoLabel->setMovie(&m_Movie);
    m_Movie.start();
}
