#ifndef CSPLASHSCREEN_H
#define CSPLASHSCREEN_H

#include <QDialog>
#include <QMovie>
#include <QTimer>

#define FLAMINGO_FILE "/tmp/RIGOL-DSO"


namespace Ui {
class CSplashScreen;
}

class CSplashScreen : public QDialog
{
    Q_OBJECT

public:
    explicit CSplashScreen(QWidget *parent = 0);
    ~CSplashScreen();
    void setLogo(QString logoName);
public slots:
    void updateTime();
private:
    Ui::CSplashScreen *ui;
    QMovie m_Movie;
    QTimer m_DateTime;
};

#endif // CSPLASHSCREEN_H
