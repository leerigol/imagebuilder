#ifndef JACKALFB_H__
#define JACKALFB_H__

#include <linux/types.h>
#include <linux/ioctl.h>

#define LAYER_NUM	 19

enum ELayerType
{
    eActiveLayer = 0,
    eStaticLayer,
    eTraceLayer,
    ePstLayer,
    eSpectroLayer,
    eRtTraceLayer,
    eMaxHoldLayer,
    eDecayLayer,
    eStaticResultLayer,
    ePingPangLayer0,
    ePingPangLayer1,
    eLayerTypeEnd,
};

struct layer_info
{
    unsigned int trace_layer_color;
    unsigned int layer_trans_opt;
    unsigned int pos_x, pos_y;
    unsigned int screen_width;
    unsigned int screen_height;
    unsigned int layer_type;
};

#define JAC_FB_IOCTL_MAGIC 'j'

#define LAYER_COMBINE                   _IOW(JAC_FB_IOCTL_MAGIC,0x01,struct layer_info)
#define RESET_COMBINE                   _IOR(JAC_FB_IOCTL_MAGIC,0x02,struct layer_info)
#define LAYER_ACTIVE_COMBINE            _IOR(JAC_FB_IOCTL_MAGIC,0x03,struct layer_info)
#define LAYER_STATIC_COMBINE            _IOR(JAC_FB_IOCTL_MAGIC,0x04,struct layer_info)
#define LAYER_COMBINE_STATE             _IOR(JAC_FB_IOCTL_MAGIC,0x05,struct layer_info)
#define SET_TRACE_LAYER_COLOR           _IOR(JAC_FB_IOCTL_MAGIC,0x06,struct layer_info)
#define SET_LAYER_OPTION                _IOR(JAC_FB_IOCTL_MAGIC,0x07,struct layer_info)
#define SET_LAYER_LOCATION              _IOR(JAC_FB_IOCTL_MAGIC,0x08,struct layer_info)
#define SET_LAYER_TYPE                  _IOR(JAC_FB_IOCTL_MAGIC,0x09,struct layer_info)
#define SET_PST_LAYER_SIZE              _IOR(JAC_FB_IOCTL_MAGIC,0x0a,struct layer_info)
#define SET_SPECTRO_LAYER_BUF_SIZE      _IOR(JAC_FB_IOCTL_MAGIC,0x0b,struct layer_info)
#define SET_SPECTRO_LAYER_SIZE          _IOR(JAC_FB_IOCTL_MAGIC,0x0c,struct layer_info)

#endif  /* JACKALFB_H__*/
