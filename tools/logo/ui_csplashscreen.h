/********************************************************************************
** Form generated from reading UI file 'csplashscreen.ui'
**
** Created by: Qt User Interface Compiler version 5.5.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CSPLASHSCREEN_H
#define UI_CSPLASHSCREEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CSplashScreen
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *logoLabel;

    void setupUi(QDialog *CSplashScreen)
    {
        if (CSplashScreen->objectName().isEmpty())
            CSplashScreen->setObjectName(QStringLiteral("CSplashScreen"));
        CSplashScreen->setWindowModality(Qt::ApplicationModal);
        CSplashScreen->resize(1024, 40);
        CSplashScreen->setStyleSheet(QStringLiteral("background-color: rgb(0, 0, 0);"));
        verticalLayout = new QVBoxLayout(CSplashScreen);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        logoLabel = new QLabel(CSplashScreen);
        logoLabel->setObjectName(QStringLiteral("logoLabel"));
        logoLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(logoLabel);


        retranslateUi(CSplashScreen);

        QMetaObject::connectSlotsByName(CSplashScreen);
    } // setupUi

    void retranslateUi(QDialog *CSplashScreen)
    {
        CSplashScreen->setWindowTitle(QApplication::translate("CSplashScreen", "Dialog", 0));
        logoLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class CSplashScreen: public Ui_CSplashScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CSPLASHSCREEN_H
