#include "csplashscreen.h"
#include <QApplication>
#include <QSplashScreen>
#include <QDateTime>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CSplashScreen w;
    if(argc == 2)
    {
        w.setLogo(argv[1]);
    }
    else
    {
        w.setLogo("/rigol/logo.gif");
    }
    w.move(0,440);
    w.show();//FullScreen();
    return a.exec();
}
