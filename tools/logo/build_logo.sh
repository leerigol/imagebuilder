bmptoppm logo.bmp > temp1.ppm
ppmquant 224 temp1.ppm > temp2.ppm
pnmnoraw temp2.ppm > logo_linux_clut224.ppm

cp logo_linux_clut224.ppm ../linux-3.12.0/drivers/video/logo/

rm temp1.ppm temp2.ppm
