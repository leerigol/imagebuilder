#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>


unsigned long simple_strtol(const char *cp, char **endp,unsigned int base)
{
	unsigned long result = 0;
	unsigned long value;

	if (*cp == '0') {
		cp++;
		if ((*cp == 'x') && isxdigit(cp[1])) {
			base = 16;
			cp++;
		}

		if (!base)
			base = 8;
	}

	if (!base)
		base = 10;

	while (isxdigit(*cp) && (value = isdigit(*cp) ? *cp-'0' : (islower(*cp)
	    ? toupper(*cp) : *cp)-'A'+10) < base) {
		result = result*base + value;
		cp++;
	}

	if (endp)
		*endp = (char *)cp;

	return result;
}
#if 0
unsigned int cmds[] = 
{
0x1c00, 0x84841,
0x1c00, 0x80a41,
0,0
};
int main(int argc, char*argv[])
{

	int tx = 0;
	int rx = 0;
	int offset = 0;
	int fd = -1;
	int count = 1;
	unsigned int*cmd = &cmds[0];
	

	fd = open("/dev/axi", O_RDWR);

	if(fd == -1)
	{
		printf("Cant' open /dev/axi\n");
		exit(1);
	}
	if(argc >= 2)
	{
		count = simple_strtol(argv[1], NULL , 10);
	}

	while(count--)
	{
		cmd = &cmds[0];

		while(1)
		{		
			offset 	=	*cmd++;
			tx		=	*cmd++;

			if(offset == 0 && tx == 0 ) break;

			ioctl(fd, 0, offset);
			if(write(fd, &tx, 4) == 4)
			{
				printf("Write:OK.%d\n",count);
			}
			else
			{
				printf("Write:Error\n");			
			}
		}
		usleep(200000);
	}
	close(fd);

	return 0;
}

#else
#define BUS_SIZE (64*1024)
int main(int argc, char*argv[])
{
	unsigned int data_base = 0;
	unsigned int data_len  = 0;
	unsigned int data_dev  = 0;
	unsigned int map_len   = 0;
	int i;
	int fd = -1;
	int write_fd;
	char *pData; 

	if( argc != 3 )
	{
		printf("usage: data (physical address of ddr) (size)\n");
		return -1;
	}
	
	data_base = simple_strtol(argv[1], NULL, 10);
	data_len  = simple_strtol(argv[2], NULL, 10);
	
	

	pData = (char*)malloc(data_len);
	if(pData == NULL)
	{
		printf("Can't malloc memory\n");
		return -1;
	}

	fd = open("/dev/mem", O_RDWR);
	if(fd == -1)
	{
	  printf("Cant' open /dev/mem\n");
	  exit(1);
	}

	printf("Reading 0x%x,size=%d\n", data_base, data_len);

	map_len = (data_len + 4095)/4096;
	data_dev = (unsigned int)mmap(NULL, map_len, PROT_READ,MAP_SHARED,fd, data_base);

	write_fd = open("./data.csv", O_RDWR|O_CREAT);
	if(write_fd >= 0 )
	{
		int i;
		char data_buf[64];

		for(i=0; i<data_len; )
		{
			unsigned int data = *(volatile unsigned int*)(data_dev + i);
			memset(data_buf, 0, sizeof(data_buf));
			sprintf(data_buf, "%d\r\n%d\r\n%d\r\n%d\r\n", data&0xff, (data>>8) & 0xff,(data>>16) & 0xff, (data>>24) & 0xff);
			write(write_fd, data_buf, strlen(data_buf));
			i+=4;
		}
		close(write_fd);
		printf("Read OK\n");
	}
	else
	{
		printf("Can't open file\n");
	}
	munmap((void*)data_dev, map_len); //destroy map memory
	close(fd);	
	return 0;
}
#endif
