#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#define BUS_SIZE (4*1024)
#define BUS_NAME "/dev/mem"
#define BUS_BASE 0x1FDA4000
#define BUS_DATA 0x44454654
int main(int argc, char*argv[])
{
	int dev_mem = -1;
	int* boot_param;
	int ret = 0;
	dev_mem = open(BUS_NAME, O_RDWR);

	if(dev_mem == -1)
	{
		printf("Open boot param failed\n");
		exit(1);
	}

	boot_param = (int*)mmap(NULL,BUS_SIZE,PROT_READ|PROT_WRITE,MAP_SHARED,dev_mem, BUS_BASE);
	if(boot_param == NULL)
	{
		printf("Map boot param failed\n");
		close(dev_mem);
		exit(1);
	}

	//printf("boot=0x%x\n", *boot_param);
	if( *boot_param == BUS_DATA )
	{
		//printf("Boot restore default\n");
		ret = 2;
	}
	

	munmap((void*)boot_param, BUS_SIZE); //destroy map memory
	close(dev_mem);
	return ret;
}
