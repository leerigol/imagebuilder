#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <sys/mount.h>

#ifndef XXTEA_TYPE
#define XXTEA_TYPE int
#endif
#define     MX      (z>>5^y<<2) + (y>>3^z<<4)^(sum^y) + (k[p&3^e]^z);
#define     XXTEA_ALIGNMENT_BYTES                 4
/***************************     ³£ÊýºÍÀàÐÍÉùÃ÷   *************************/

/***************************     ±äÁ¿ÉùÃ÷         *************************/

/***************************     º¯ÊýÔ­ÐÎ        **************************/

/***************************     º¯ÊýÊµÏÖ        **************************/


/**
 *	@brief	XXTEA¹Ù·½Ëã·¨£¬´ËÀà¼´ÊÇ¶ÔÆä½øÐÐ·â×°£¬Ïê¼ûhttp://en.wikipedia.org/wiki/XXTEA
 *
 *	@param 	v 	¼Ó½âÃÜÊý¾ÝÁ÷
 *	@param 	n 	¼Ó½âÃÜ³¤¶È£¬n > 1Îª¼ÓÃÜ£¬n < -1Îª½âÃÜ
 *	@param 	k 	ÃÜÔ¿      ×¢Òâ£ºÐÎÊ½²ÎÊýkÐèºÍMXÖÐµÄk¶ÔÓ¦
 *
 *	@return	·µ»Ø0±íÊ¾¼Ó½âÃÜ³É¹¦£¬·µ»Ø1±íÊ¾Ê§°Ü
*/
long btea(XXTEA_TYPE* v, long n, const XXTEA_TYPE* k)
{
    unsigned XXTEA_TYPE z=v[n-1], y=v[0];
    unsigned long sum=0, e, DELTA=0x9e3779b9;
    long p, q ;
    if (n > 1)
	{          /* Coding Part */
        q = 6 + 52/n;
        while (q-- > 0) {
            sum += DELTA;
            e = (sum >> 2) & 3;
            for (p=0; p<n-1; p++) y = v[p+1], z = v[p] += MX;
            y = v[0];
            z = v[n-1] += MX;
        }
        return 0 ;
    } else if (n < -1)
    {  /* Decoding Part */
        n = -n;
        q = 6 + 52/n;
        sum = q*DELTA ;
        while (sum != 0) {
            e = (sum >> 2) & 3;
            for (p=n-1; p>0; p--) z = v[p-1], y = v[p] -= MX;
            z = v[n-1];
            y = v[0] -= MX;
            sum -= DELTA;
        }
        return 0;
    }
    return 1;
}

/**
*	@brief	¶ÔÊý¾ÝÁ÷½øÐÐ¼ÓÃÜ,ÊäÈë±ØÐë°´DWORD¶ÔÆë,¼´Ö»Õë¶ÔDWORDÊý¾Ý½øÐÐ¼ÓÃÜ
*
*	@param 	lpDstBuffer 	Ä¿±ê»º³åÇø
*	@param 	lpSrcBuffer 	Ô´»º³åÇø
*	@param 	nLength 	Ô´»º³åÇø³¤¶È
*	@param 	lpKey 	ÃÜÔ¿
*
*	@return	¼ÓÃÜÊÇ·ñ³É¹¦
*/
int XXTEAEncode( int* lpDstBuffer,
				 const int* lpSrcBuffer,
				 size_t nLength, 						//DWORDÊýÁ¿
				 const XXTEA_TYPE* lpKey)
{
    long ret = 1;
    if (nLength > 1 && lpDstBuffer && lpSrcBuffer
		&& ( ((int)lpSrcBuffer & 0x03) == 0 )      //±£Ö¤4×Ö½Ú¶ÔÆë
		&& ( ((int)lpDstBuffer & 0x03) == 0 )
                  )
	{
														//Êý¾Ý»º´æ
		memcpy( lpDstBuffer, lpSrcBuffer, nLength * sizeof(int) );

														//¼ÓÃÜ¹ý³Ì
        ret = btea((XXTEA_TYPE*)lpDstBuffer, nLength, lpKey);
    }

	return ret;
}

/**
*	@brief	¶ÔÊý¾ÝÁ÷½øÐÐ½âÃÜ,ÊäÈë±ØÐë°´DWORD¶ÔÆë,Ö»Õë¶ÔDWORDÊý¾Ý½øÐÐ½âÃÜ
*
*	@param 	lpDstBuffer 	Ä¿±ê»º³åÇø
*	@param 	lpSrcBuffer 	Ô´»º³åÇø
*	@param 	nLength 	Ô´»º³åÇø³¤¶È
*	@param 	lpKey 	ÃÜÔ¿
*
*	@return	½âÃÜÊÇ·ñ³É¹¦
*/
int XXTEADecode( int* lpDstBuffer,
				  int* lpSrcBuffer,
				 size_t nLength,						//DWORDÊýÁ¿
				 const XXTEA_TYPE* lpKey)
{
    long ret = 1;
    if (nLength > 1 && lpDstBuffer && lpSrcBuffer
			&& ( ((int)lpSrcBuffer & 0x03) == 0 )
			&& ( ((int)lpDstBuffer & 0x03) == 0 )
	    )
	{
		memcpy( lpDstBuffer, lpSrcBuffer, nLength * sizeof(int) );

		ret = btea((XXTEA_TYPE*)lpDstBuffer, -nLength, lpKey);
    }
    return ret;
}

int _keys[] =
{
	0x03920001,
	0x08410841,
	0x18c32104,
	0x318639c7
};

int deCrypt_data(int *input_data, int *deCrypt_data_buf)
{
    //ÅÐ¶Ïinput_dataÊÇ·ñÊÇ4×Ö½Ú¶ÔÆë
    return (XXTEADecode(deCrypt_data_buf, input_data, 128, _keys));
}

int Crypt_data(int *crypt_data_buf)
{
    int _bret = 1;
    char *temp_data = (char*)malloc(512);
	char *limited_data = "RIGOL TECHNOLOGIES,DS1000Z,SPARROW,201212";
    memset(temp_data, 0x00, 512);
    memcpy(temp_data, limited_data, strlen(limited_data));
    //ÅÐ¶Ïtemp_dataÊÇ·ñÊÇ4×Ö½Ú¶ÔÆë
    _bret = XXTEAEncode(crypt_data_buf, (int*)temp_data, 128, _keys);
    free(temp_data);
    return (_bret);
}

void print_sector(char* buf)
{
	int line;
	int j;
	for(line=0; line<32; line++)  
    {  
        for(j=0; j<16; j++)  
        {  
            printf("%02x ", (unsigned char)buf[line*16+j]);  
        }  
        printf("\n");  
    }  
}

void write_sector( char *dev_name )
{
	char buf[1000] = {0};
	char srcMsg[1000];

	int fd = open(dev_name, O_RDWR);
	if( fd != -1 )
	{
		int hide_sector = 0;
		int sizes = 0;

        ioctl(fd, BLKSSZGET, &sizes);  
        printf("sector size=%d\n", sizes);

		lseek(fd, 0, SEEK_SET);  
        read(fd, buf, sizes); 

		hide_sector = buf[0xf] ;
		hide_sector = (( hide_sector << 8 ) | buf[0xe] ) - 1;

		lseek(fd, hide_sector*512, SEEK_SET);

		memset(srcMsg, 0, sizeof(srcMsg));
		Crypt_data((int*)srcMsg);
		write(fd, srcMsg, sizes); 
		
		close(fd);
	}
}

void read_sector(char *dev_name )
{
 	int fd=0;  
    int sizes = 0;  
    char buf[1000] = {0};  
     int line, j;  
	char srcMsg[1000];
  
    fd = open(dev_name, O_RDONLY);  
    if(fd !=-1)  
    {     
		int hide_sector = 0;          
        ioctl(fd, BLKSSZGET, &sizes);  
        printf("sector size=%d\n", sizes);  
  
        lseek(fd, 0, SEEK_SET);  
        read(fd, buf, sizes);  
		print_sector(buf);

		hide_sector = buf[0xf] ;
		hide_sector = (( hide_sector << 8 ) | buf[0xe] ) - 1;

		printf("Hide Sector =%d,%x,%x\n", hide_sector, buf[0xe], buf[0xf]);

		lseek(fd, hide_sector*512, SEEK_SET);
        read(fd, buf, sizes); 

		memset(srcMsg, 0, sizeof(srcMsg));
		deCrypt_data((int*)buf, (int*)srcMsg);

		printf("Original message is:%s\n", srcMsg);
		close(fd);
    }  
}

int main(int argc, char** argv)   
{  
    write_sector("/dev/sda1");
	read_sector("/dev/sda1");
  
    return (0);  
}  
